

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var productsCollectionView: UICollectionView! {
        didSet {
            productsCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var order: OrderModel! {
        didSet {
            productsCollectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(for order: OrderModel) {
        dateLabel.text = order.createdAt
        paymentStatusLabel.text = order.isPayment == true ? "Оплачен" : "Не оплачен"
        summLabel.text = "\(order.total) р."
        countLabel.text = "\(order.products.count) шт."
        paymentLabel.text = order.paymentText
        deliveryLabel.text = order.deliveryText
        orderStatusLabel.text = order.status
        orderLabel.text = "Заказ №\(order.number!)"
        self.order = order
    }
    
    
}

extension OrderTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if order != nil {
            return order!.images.count
        }
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.configure(for: order!.images[indexPath.item].url)
        return cell
    }
    
}
