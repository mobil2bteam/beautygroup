
import UIKit

class GroupTableViewCell: UITableViewCell {

    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    
    var clickClosure: ((CatalogModel) -> ())?
    var moreClosure: ((GroupModel) -> ())?

    let identifier = String(describing: ItemCollectionViewCell.self)
    var group: GroupModel? {
        didSet {
            itemsCollectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemsCollectionView.backgroundColor = UIColor.colorBackgroundDark
        itemsCollectionView.register(ItemCollectionViewCell.nib(), forCellWithReuseIdentifier: identifier)
        itemsCollectionView.dataSource = self
        itemsCollectionView.delegate = self
    }

    func configure(for group: GroupModel) {
        moreButton.isHidden = !group.showButtonMore
        groupLabel.text = group.name
        moreButton.setTitle(group.moreText, for: .normal)
        moreButton.setTitleColor(UIColor.colorAccent, for: .normal)
        self.group = group
        (itemsCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = group.scrollDirection == .vertical ? .vertical : .horizontal
        itemsCollectionView.isScrollEnabled = group.scrollDirection == .horizontal
        itemsCollectionView.reloadData()
    }

    @IBAction func moreButtonClicked(_ sender: Any) {
        guard let closure = moreClosure, let group = self.group else {
            return
        }
        closure(group)
    }
}


extension GroupTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if group != nil {
            return group!.items.count
        }
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! ItemCollectionViewCell
        cell.configure(for: group!.items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if clickClosure != nil {
            clickClosure!(group!.items[indexPath.item])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / group!.itemsDisplay
        return CGSize(width: width, height: ItemCollectionViewCell.itemHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

