

import UIKit

class Cart2TableViewCell: CartTableViewCell {

    @IBOutlet weak var totalLabel: MainLabel!

    override func configure(for item: CatalogModel) {
        super.configure(for: item)
        if item.prices.count > 0 {
            let price = item.prices[0]
            totalLabel.text = "\(price.price * CartManager.count(for: item.id)) р."
        }
    }

    func configureOrder(for item: CatalogModel) {
        super.configure(for: item)
        if let price =  item.prices.first {
            totalLabel.text = "\(price.count * price.price) р."
            countLabel.text = "\(price.count)"
        }
        oldPriceLabel.text = ""
        saleLabel.isHidden = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        totalLabel.text = ""
    }
}
