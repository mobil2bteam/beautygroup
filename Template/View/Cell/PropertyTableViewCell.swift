

import UIKit

class PropertyTableViewCell: UITableViewCell {

    @IBOutlet var dotsViewArray: [UIView]!
    @IBOutlet weak var propertyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(for property: ProductPropertyModel) {
        propertyLabel.text = property.name
        if let value = Int(property.value) {
            dotsViewArray.forEach {
                $0.backgroundColor = $0.tag <= value ? UIColor.colorPrimary : UIColor.colorBackgroundDark
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dotsViewArray.forEach { $0.backgroundColor = UIColor.colorBackgroundDark }
        propertyLabel.text = ""
    }
}
