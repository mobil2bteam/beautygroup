
import UIKit

class PriceTableViewCell: UITableViewCell {

    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!

    func configure(for price: PriceModel) {
        priceLabel.text = "\(price.price) р."
        oldPriceLabel.text = ""
        priceLabel.textColor = UIColor.colorTextPrice
        weightLabel.text = price.weight
        if let oldPrice = price.oldPrice {
            if oldPrice == 0 {
                return
            }
            let text = "\(oldPrice)"
            let attrString = NSAttributedString(string: text, attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
            oldPriceLabel.attributedText = attrString
            priceLabel.textColor = UIColor.colorTextDiscountPrice
            oldPriceLabel.textColor = UIColor.colorTextOldPrice
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        priceLabel.text = ""
        oldPriceLabel.text = ""
    }

}
