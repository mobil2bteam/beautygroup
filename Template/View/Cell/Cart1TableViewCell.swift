
import UIKit

class Cart1TableViewCell: CartTableViewCell {

    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        for b in [minusButton, plusButton, deleteButton] {
            b?.tintColor = UIColor.colorTextPrimary
        }
    }

    override func configure(for item: CatalogModel) {
        super.configure(for: item)
    }

    @IBAction func deleteButtonClicked(_ sender: Any) {
        deleteHandler!(item!)
    }
    
    @IBAction func plusButtonClicked(_ sender: Any) {
        CartManager.increaseProduct(priceId: item!.prices.first?.id ?? 0)
    }
    
    @IBAction func minusButtonClicked(_ sender: Any) {
        if CartManager.count(for: item!.prices.first?.id ?? 1) > 1 {
            CartManager.decreaseProduct(priceId: item!.prices.first?.id ?? 0)
        }
    }
}
