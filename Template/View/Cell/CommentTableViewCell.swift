
import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(for comment: CommentModel) {
        cityLabel.text = comment.user.info.cityName
        ratingLabel.text = comment.rating
        userLabel.text = comment.user.info.userName
        commentLabel.text = comment.text
        dateLabel.text = comment.date
        let rating = (comment.rating as NSString).floatValue
        ratingLabel.backgroundColor = CommentModel.color(for: CGFloat(rating))
    }
}
