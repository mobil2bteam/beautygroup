
import UIKit

class SocialCollectionViewCell: UICollectionViewCell {
    
    var url: String?
    
    @IBOutlet weak var socialButton: UIButton!
    
    @IBAction func socialButtonClicked(_ sender: Any) {
        if url != nil {
            openURL(urlString: url!)
        }
    }
    
    //MARK: Helper
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
