
import UIKit

class PointTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(for point: PointModel) {
        nameLabel.text = point.name
        addressLabel.text = point.contact.address.street
        phoneLabel.text = point.contact.phone
    }
    
}
