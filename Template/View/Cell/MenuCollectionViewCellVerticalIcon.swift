
import UIKit
import AlamofireImage

class MenuCollectionViewCellVerticalIcon: MenuCollectionViewCell {

    @IBOutlet var textLabel: UILabel!
    @IBOutlet var imageView: UIImageView!

    override func configure(for menuItem: MenuItemModel) {
        textLabel.textColor = UIColor(menuItem.textColor)
        textLabel.text = menuItem.name
        guard let image = menuItem.icon.url,
            let url =  URL(string: image) else {
                imageView.image = nil
                return
        }
        imageView.af_setImage(withURL: url)
    }
    
}
