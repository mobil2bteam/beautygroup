
import UIKit

class DeliveryTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentLabel.textColor = UIColor.colorTextAccent
    }
    
    func configure(for delivery: DeliveryModel) {
        nameLabel.text = delivery.name
        if delivery.price > 0 {
            priceLabel.text = "+\(delivery.price) р."
        } else {
            priceLabel.text = ""
        }
        let payments = delivery.payments.map { (payment) -> String in
            return payment.name
        }
        paymentLabel.text = payments.joined(separator: ", ")
        if delivery.addText.isEmpty {
            commentLabel.isHidden = true
        } else {
            commentLabel.text = delivery.addText
            commentLabel.isHidden = false
        }
    }
}
