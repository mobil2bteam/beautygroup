
import RealmSwift

class RecipientDataModel: Object{
    
    dynamic var id = 0
    dynamic var date = Date()
    dynamic var name = ""
    dynamic var phone = ""

    override static func primaryKey() -> String? {
        return "id"
    }
    
}



