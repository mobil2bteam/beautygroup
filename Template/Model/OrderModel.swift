
import ObjectMapper

class OrdersModel: Mappable {
    
    var archive: [OrderModel] = []
    var actual: [OrderModel] = []
    
    func allOrders() -> [OrderModel] {
        return actual + archive
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        archive                     <- map["archive"]
        actual                      <- map["actual"]
    }
}


class OrderModel: Mappable {
    
    var id: Int!
    var number: Int!
    var createdAt: String = ""
    var userId: Int!
    var balls: Int!
    var isPayment: Bool! = false
    var totalNotSale: Int = 0
    var sumSale: Int = 0
    var percentSale: Int = 0
    var cartSale: Int = 0
    var promoPercent: Int = 0
    var totalSale: Int = 0
    var totalSumSale: Int = 0
    var totalSumPercent: Int = 0
    var totalCart: Int = 0
    var sumDelivery: Int = 0
    var total: Int = 0
    var products: [CatalogModel] = []
    var images: [ImageModel] = []
    //var status: OrderStatusModel?
    var status: String! = ""
    var address: String! = ""
    var deliveryText: String! = ""
    var paymentText: String! = ""
    var delivery: DeliveryOrderModel?
    var payment: PaymentOrderModel?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                          <- map["id"]
        number                      <- map["number"]
        createdAt                   <- map["createdAt"]
        isPayment                   <- map["isPayment"]
        userId                      <- map["userId"]
        balls                       <- map["balls"]
        products                    <- map["products"]
        images                      <- map["products"]
        totalNotSale                <- map["totalNotSale"]
        sumSale                     <- map["sumSale"]
        percentSale                 <- map["percentSale"]
        cartSale                    <- map["cartSale"]
        totalSale                   <- map["totalSale"]
        totalSumSale                <- map["totalSumSale"]
        totalSumPercent             <- map["totalSumPercent"]
        totalCart                   <- map["totalCart"]
        sumDelivery                 <- map["sumDelivery"]
        total                       <- map["total"]
        promoPercent                <- map["promoPercent"]
        status                      <- map["status"]
        deliveryText                <- map["deliveryText"]
        paymentText                 <- map["paymentText"]
        delivery                    <- map["delivery"]
        payment                     <- map["payment"]
        address                     <- map["contact_user.address"]
    }
}


class OrderStatusModel: Mappable {
    
    var statusId: Int!
    var status: String = ""
    var statusColor: String = ""
    var statusColorText: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        statusId                         <- map["statusId"]
        status                           <- map["status"]
        statusColor                      <- map["statusColor"]
        statusColorText                  <- map["statusColorText"]
    }
    
}


class DeliveryOrderModel: Mappable {
    
    var deliveryId: Int!
    var deliveryType: DeliveryType!
    var deliveryDate: String!
    var deliveryText: String = ""
    var addressPoint: String = ""

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        deliveryId                         <- map["deliveryId"]
        deliveryType                       <- map["deliveryType"]
        deliveryDate                       <- map["deliveryDate"]
        addressPoint                       <- map["addressPoint"]
        deliveryText                       <- map["deliveryText"]
    }
    
}


class PaymentOrderModel: Mappable {
    
    var paymentId: Int!
    var paymentType: PaymentType!
    var paymentText: String = ""
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        paymentId                          <- map["paymentId"]
        paymentType                        <- map["paymentType"]
        paymentText                        <- map["paymentText"]
    }
    
}

