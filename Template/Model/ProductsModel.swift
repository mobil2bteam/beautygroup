
import ObjectMapper

class ProductsModel: Mappable {

    var filter: FilterModel!
    var items: [CatalogModel] = []
    var title: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        items                       <- map["products.items"]
        filter                      <- map["products.filter"]
        title                       <- map["products.options.title"]
    }
    
}

class FilterModel: Mappable {

    var catalogId: Int!
    var catalogs: [BaseModel] = []
    var countries: [Int] = []
    var brandId: [Int] = []
    var brands: [BrandModel] = []
    var sizes: [Int] = []
    var seasons: [Int] = []
    var colors: [Int] = []
    var materials: [Int] = []
    var flags: [Int] = []
    var ages: [Int] = []
    var sale: Bool = false
    var priceFrom: Int = 0
    var priceTo: Int = 0
    var sexId: Int = 0
    var sortId: Int = 0
    var search: String = ""
    var page: Int!
    var pageCount: Int!
    var onPage: Int!
    var canLoadMore: Bool {
        return page < pageCount
    }
    
    var formatted: [FormattedFilterModel]?

    var quickFilters: [BaseModel] {
        get {
            if catalogs.count > 0 {
                return catalogs
            }
            if AppManager.shared.options.settings.filter.brand && brands.count > 1 {
                return brands
            }
            return []
        }
    }
    
    func formattedFilters(for options: OptionsModel) -> [FormattedFilterModel] {
        if formatted != nil {
            return formatted!
        }
        var temp = [FormattedFilterModel]()
        let filter = options.settings.filter!
        let directories = options.directory
        
        if filter.country {
            for i in directories!.countries {
                i.checked = countries.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Страна производитель", filter: directories!.manufactures))
        }
        
        if filter.brand {
            for i in directories!.brands {
                i.checked = brandId.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Бренд", filter: directories!.brands))
        }

        if filter.size {
            for i in directories!.sizes {
                i.checked = sizes.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Размер", filter: directories!.sizes))
        }
        
        if filter.season {
            for i in directories!.seasons {
                i.checked = seasons.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Сезон", filter: directories!.seasons))
        }

        if filter.color {
            for i in directories!.colors {
                i.checked = colors.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Цвет", filter: directories!.colors))
        }
        
        if filter.material {
            for i in directories!.materials {
                i.checked = materials.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Материал", filter: directories!.materials))
        }
        
        if filter.flags {
            for i in directories!.flags {
                i.checked = flags.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Метки", filter: directories!.flags))
        }
        
        if filter.age {
            for i in directories!.ages {
                i.checked = ages.contains(i.id)
            }
            temp.append(FormattedFilterModel(name: "Возраст", filter: directories!.ages))
        }
        formatted = temp
        return temp
    }
    
    func filterParams() -> [String: String] {
        var params = [String: String]()
        if !search.isEmpty {
            params["search"] = search
        }
        if catalogId != 0 {
            params["catalog_id"] = "\(catalogId!)"
        }
        if priceFrom != 0 {
            params["price_from"] = "\(priceFrom)"
        }
        if priceTo != 0 {
            params["price_to"] = "\(priceTo)"
        }
        if sale {
            params["sale"] = "1"
        }
        
        params["sort_id"] = "\(sortId)"
        if AppManager.shared.options.settings.filter.sex {
            params["sex_id"] = "\(sexId)"
        }
        let temp = formattedFilters(for: AppManager.shared.options)
        if temp.count > 0 {
            for f in temp {
                var a = [String]()
                for b in f.filter {
                    if b.checked {
                        a.append("\(b.id)")
                    }
                }
                let value: String = a.joined(separator: "-")
                if value.characters.count > 0 {
                    switch f.name {
                    case "Страна производитель":
                        params["country_manufacture_id"] = value
                    case "Бренд":
                        params["brand_id"] = value
                    case "Размер":
                        params["size_id"] = value
                    case "Сезон":
                        params["season_id"] = value
                    case "Цвет":
                        params["color_id"] = value
                    case "Материал":
                        params["material_id"] = value
                    case "Метки":
                        params["flag_id"] = value
                    case "Возраст":
                        params["age_id"] = value
                    default:
                        break
                    }
                }
            }
        }
        return params
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        catalogId               <- map["catalog_id"]
        brandId                 <- map["brand_id"]
        brands                  <- map["brand_list"]
        catalogs                <- map["catalog_list"]
        countries               <- map["country_manufacture_id"]
        sizes                   <- map["size_id"]
        seasons                 <- map["season_id"]
        colors                  <- map["color_id"]
        materials               <- map["material_id"]
        flags                   <- map["flag_id"]
        ages                    <- map["age_id"]
        sale                    <- map["sale"]
        priceFrom               <- map["price_from"]
        priceTo                 <- map["price_to"]
        sexId                   <- map["sex_id"]
        sortId                  <- map["sort_id"]
        search                  <- map["search"]
        page                    <- map["page"]
        pageCount               <- map["count_page"]
        onPage                  <- map["onpage"]
    }
}

class FormattedFilterModel {
    
    var name: String!
    var filter: [BaseModel] = []
    
    init(name: String, filter: [BaseModel]) {
        self.name = name
        self.filter = filter
    }
    
    func prepare() {
        for f in filter {
            f.tempChecked = f.checked
        }
    }
    
    func formattedValue() -> String {
        var temp = [String]()
        for f in filter {
            if f.checked {
                temp.append(f.name)
            }
        }
        if temp.count == 0 || temp.count == filter.count{
            return "Все значения"
        }
        return temp.joined(separator: ", ")
    }
}

