
import ObjectMapper

class SettingsModel: Mappable {
    var selectStartCity: Bool!          /// спрашивать у пользователя город при старте
    var changeCity: Bool!               /// возможность смены города
    var enterIsNeeded: Bool! = false
    var paymentOrderAfterSubmit: Bool! = false

    var defaultСity: Int!               /// город по умолчанию
    var defaultСatalog: Int!            /// каталог по умолчанию
    var company: CompanyModel!
    var filter: FilterSettingModel!
    var menus: [MenuInfoModel] = []
    var partnerInfo: MenuInfoModel!

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        selectStartCity             <- map["select_start_city"]
        changeCity                  <- map["change_city"]
        defaultСity                 <- map["default_city"]
        defaultСatalog              <- map["default_catalog"]
        company                     <- map["company_info"]
        filter                      <- map["filters"]
        menus                       <- map["menu_info"]
        paymentOrderAfterSubmit     <- map["flag_payment_order_after"]
        enterIsNeeded               <- map["flag_closed_type_app"]
        partnerInfo                 <- map["partner_info"]
    }
}

class FilterSettingModel: Mappable {
    var brand: Bool!
    var size: Bool!
    var season: Bool!
    var color: Bool!
    var material: Bool!
    var age: Bool!
    var flags: Bool!
    var price: Bool!
    var sex: Bool!
    var country: Bool!
    var properties: Bool!

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        brand                   <- map["brand"]
        size                    <- map["size"]
        season                  <- map["season"]
        color                   <- map["color"]
        material                <- map["material"]
        age                     <- map["age"]
        flags                   <- map["flags"]
        price                   <- map["price"]
        sex                     <- map["sex"]
        country                 <- map["country"]
        properties              <- map["properties"]
    }
}

class CompanyModel: Mappable {
    var id: Int!
    var name: String!
    var descriptionShort: String?
    var descriptionLong: String?
    var social: SocialModel?
    var slider: SliderModel?
    var contact: ContactModel!
    var workingHours: [WorkModel] = []
    var points: [PointModel] = []
    
    var minPriceForPoints: Int {
        let city = CityManager.getDeliveryCity().name
        let cityPoints = points(for: city)
        var price = points.first?.summDelivery ?? 0
        cityPoints.forEach{ if $0.summDelivery < price {
                price = $0.summDelivery
            }
        }
        return price
    }
    
    var maxPriceForPoints: Int {
        let city = CityManager.getDeliveryCity().name
        let cityPoints = points(for: city)
        var price = points.first?.summDelivery ?? 0
        cityPoints.forEach{ if $0.summDelivery > price {
                price = $0.summDelivery
            }
        }
        return price
    }

    func pointsCities() -> [String] {
        var cities = [String]()
        for p in points {
            if !cities.contains(p.contact.address.city) {
                if p.checkPointOffice || p.checkPointSale {
                    cities.append(p.contact.address.city)
                }
            }
        }
        cities.sort()
        return cities
    }
    
    func points(for city: String = CityManager.getDeliveryCity().name) -> [PointModel] {
        return points.filter{ $0.contact.address.city == city && ($0.checkPointOffice || $0.checkPointSale) }
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        descriptionShort        <- map["description_short"]
        descriptionLong         <- map["description_long"]
        social                  <- map["social_links"]
        slider                  <- map["slider"]
        workingHours            <- map["working_house"]
        points                  <- map["points"]
        contact                 <- map["contacts"]
    }
}

class WorkModel: Mappable {
    var freeDay: Bool!
    var name: String!
    var hoursStart: String!
    var hoursFinish: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        freeDay                         <- map["free_day"]
        name                            <- map["day"]
        hoursStart                      <- map["hours_start"]
        hoursFinish                     <- map["hours_finish"]
    }
}

class MenuInfoModel: Mappable {
    
    var id: Int!
    var descriptionShort: String! = ""
    var descriptionLong: String! = ""
    var name: String!
    var url: String?
    var image: ImageModel?
    
    required init?(map: Map) {
        
    }
    
    func newsItems() -> NewsItemModel {
        let news = NewsItemModel.init(map: Map.init(mappingType: .fromJSON, JSON: [:]))!
        news.image = image
        news.addText = descriptionShort
        news.text = descriptionLong
        news.name = name
        news.url = url ?? ""
        return news
    }
    
    func mapping(map: Map) {
        id                              <- map["id"]
        name                            <- map["name"]
        descriptionShort                <- map["description_short"]
        descriptionLong                 <- map["description_long"]
        image                           <- map["image"]
        url                             <- map["url"]
    }
}

class PointModel: Mappable {
    var id: Int!
    var descriptionShort: String! = ""
    var descriptionLong: String! = ""
    var name: String!
    var social: SocialModel?
    var contact: ContactModel!
    var workingHours: [WorkModel] = []
    var slider: SliderModel?
    var summDelivery: Int!
    var summFreeDelivery: Int!
    var checkPointSale: Bool!
    var checkPointOffice: Bool!
    var checkPointPickup: Bool!
    var checkPointCash: Bool!
    var checkPointDressing: Bool!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                                  <- map["id"]
        name                                <- map["name"]
        descriptionShort                    <- map["description_short"]
        descriptionLong                     <- map["description_long"]
        social                              <- map["social_links"]
        contact                             <- map["contacts"]
        workingHours                        <- map["working_house"]
        slider                              <- map["slider"]
        summDelivery                        <- map["summ_delivery"]
        summFreeDelivery                    <- map["summ_free_delivery"]
        checkPointSale                      <- map["check_point_sale"]
        checkPointOffice                    <- map["check_point_office"]
        checkPointPickup                    <- map["check_point_pickup"]
        checkPointCash                      <- map["check_point_cache"]
        checkPointDressing                <- map["check_point_dressing"]
    }
}


class ContactModel: Mappable {
    var phone: String! = ""
    var email: String! = ""
    var address: AddressModel!

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        phone                           <- map["phone"]
        email                           <- map["email"]
        address                         <- map["address"]
    }
}

class GeoModel: Mappable {
    var lat: Double! = 0
    var lng: Double! = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        lat                           <- map["lat"]
        lng                           <- map["lng"]
    }
}

class AddressModel: Mappable {
    var city: String! = ""
    var metro: String! = ""
    var street: String! = ""
    var comment: String! = ""
    var cityId: Int!
    var metroId: Int!
    var geo: GeoModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        city                            <- map["city"]
        metro                           <- map["metro"]
        comment                         <- map["comment"]
        street                          <- map["street"]
        cityId                          <- map["city_id"]
        metroId                         <- map["metro_id"]
        geo                             <- map["geo"]
    }
}


class SocialModel: Mappable {
    var youtube: String = ""
    var vk: String = ""
    var facebook: String = ""
    var instagram: String = ""
    var twitter: String = ""
    var ok: String = ""
    var web: String = ""

    func socialSource() -> [[String: String]] {
        var temp = [[String: String]]()
        if !facebook.isEmpty {
            temp.append(["url": facebook, "image":"icons8-facebook-528.png"])
        }
        if !vk.isEmpty {
            temp.append(["url": vk, "image":"icons8-vk.com-528.png"])
        }
        if !instagram.isEmpty {
            temp.append(["url": instagram, "image":"icons8-instagram-528.png"])
        }
        if !youtube.isEmpty {
            temp.append(["url": youtube, "image":"icons8-youtube-528.png"])
        }
        if !twitter.isEmpty {
            temp.append(["url": twitter, "image":"icons8-twitter-528.png"])
        }
        if !ok.isEmpty {
            temp.append(["url": ok, "image":"icons8-odnoklassniki-528.png"])
        }
        return temp
    }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        youtube             <- map["youtube"]
        vk                  <- map["vk"]
        facebook            <- map["facebook"]
        instagram           <- map["instagram"]
        twitter             <- map["twitter"]
        ok                  <- map["ok"]
        web                 <- map["web"]
    }
}

