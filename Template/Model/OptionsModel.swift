
import ObjectMapper

class OptionsModel: Mappable {
    var cache: Int?
    var urls: [UrlModel]!
    var directory: DirectoryModel!
    var home: HomeModel!
    var settings: SettingsModel!
    
    func url(for name: String) -> UrlModel? {
        for u in urls {
            if u.name == name {
                return u
            }
        }
        return nil
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        cache           <- map["cache"]
        urls            <- map["options.urls"]
        directory       <- map["directories"]
        home            <- map["home"]
        settings        <- map["settings"]

    }
}

class UrlModel: Mappable {
    var name: String!
    var url: String!
    var method: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name        <- map["name"]
        url         <- map["url"]
        method      <- map["metod"]
    }
}
