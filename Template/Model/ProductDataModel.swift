
import RealmSwift

class ProductDataModel: Object {
    
    dynamic var id = 0
    dynamic var date = Date()
    dynamic var name = ""
    dynamic var image = ""
    dynamic var isFavorite = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

