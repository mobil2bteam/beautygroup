
import RealmSwift

class CartProductDataModel: Object{
    
    dynamic var priceId: Int = 0
    dynamic var count: Int = 0
    dynamic var cart: String = ""

    override static func primaryKey() -> String? {
        return "priceId"
    }
}



