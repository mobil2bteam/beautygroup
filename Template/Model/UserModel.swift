
import ObjectMapper

class UserModel: Mappable {
    
    var info: UserInfoModel!
    var priceType: PriceTypeModel!
    var cards: [CardModel] = []
    var promoCode:PromoCodeModel?
    var isSocail: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "social")
        }
    }
    
    var card: CardModel? {
        get {
            return cards.filter{ $0.type == .discount }.first
        }
    }
    var ballCard: CardModel? {
        get {
            return cards.filter{ $0.type == .ball }.first
        }
    }

    var balls: Int {
        get {
            return cards.filter{ $0.type == .ball}.first?.balance ?? 0
        }
    }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        info                       <- map["info"]
        cards                      <- map["cards"]
        priceType                  <- map["price_type"]
        promoCode                  <- map["promocode"]
    }
    
}

class UserInfoModel: Mappable {

    var active:Bool!
    var sex:Bool!
    var verify:Bool! = false
    var partner:Bool! = false
    var partnerFeed:Bool! = false
    var token: String!
    var cityName: String!
    var cityId: Int!
    var email: String!
    var userName: String!
    var firstName: String!
    var lastName: String!
    var secondName: String!
    var ordersCount: Int!
    var phone: String!

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        active                     <- map["active"]
        sex                        <- map["sex"]
        partner                    <- map["partner"]
        partnerFeed                <- map["partner_feed"]
        verify                     <- map["verify"]
        token                      <- map["token"]
        cityName                   <- map["city_name"]
        cityId                     <- map["city_id"]
        email                      <- map["email"]
        userName                   <- map["user_name"]
        firstName                  <- map["first_name"]
        lastName                   <- map["last_name"]
        secondName                 <- map["second_name"]
        ordersCount                <- map["orders_count"]
        phone                      <- map["phone"]
    }
    
}



class PriceTypeModel: Mappable {
    
    var id:Int!
    var name: String!
    var descriptionShort: String!
    var image: ImageModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                         <- map["id"]
        name                       <- map["name"]
        descriptionShort           <- map["description_short"]
        image                      <- map["image"]
    }
    
}

public enum CardType: String {
    
    case ball               = "ball"
    case discount           = "discount"
    
    init(type: String) {
        self = CardType(rawValue: type) ?? .discount
    }
    
}

class CardModel: Mappable {
    
    var id:Int!
    var balance:Int! = 0
    var cashback:Int!
    var maxPercent:Int!
    var relation:Int!
    var number: String!
    var type: CardType!

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                              <- map["id"]
        number                          <- map["number"]
        balance                         <- map["balance"]
        cashback                        <- map["cashback"]
        maxPercent                      <- map["max_percent"]
        relation                        <- map["relation"]
        type                            <- map["type"]
    }
    
}
