
import ObjectMapper

class CatalogModel: Mappable, Product {
    var id: Int!
    var image: ImageModel?
    var icon: ImageModel!
    var name: String = ""
    var flags: [FlagModel] = []
    var rating: Int!
    var commentCount: Int!
    var prices: [PriceModel] = []
    var items: [CatalogModel] = []
    var view: CatalogStyle = .text
    var search: Bool = false // показывает, нужно ли искать сразу товары при переходе в подкаталог, если false, то показываются подкаталоги (если они есть)
    var notShowName: Bool = false
    var comment = ""

    var showSubcatalogOnClick: Bool {
        return items.count > 0 && !search
    }
    
    var salePercent: Int {
        get {
            var sale = 0
            for p in prices {
                if p.sumSalePercent > 0 {
                    if p.sumSalePercent > sale {
                        sale = p.sumSalePercent
                    }
                } else {
                    if let oldPrice = p.oldPrice {
                        if oldPrice > 0 {
                            let s = (1.0 - CGFloat(p.price) / CGFloat(p.oldPrice!)) * 100
                            if Int(ceil(s)) > sale {
                                sale = Int(ceil(s))
                            }
                        }
                    }
                }
            }
            return sale
        }
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        prices                  <- map["prices"]
        commentCount            <- map["commentCount"]
        rating                  <- map["rating"]
        flags                   <- map["flags"]
        image                   <- map["image"]
        icon                    <- map["icon"]
        items                   <- map["items"]
        view                    <- map["view"]
        search                  <- map["search"]
        comment                 <- map["comment"]
        notShowName             <- map["flag_not_show_name"]
    }
}

class PriceModel: Mappable {
    var id: Int!
    var oldPrice: Int?
    var price: Int = 0
    var count: Int = 0
    var stock: Bool!
    var intValue: Int!
    var sumSalePercent: Int! = 0
    var stringValue: String!
    var weight: String! = ""
    var str: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                 <- map["id"]
        oldPrice           <- map["old_price"]
        price              <- map["price"]
        count              <- map["count"]
        stock              <- map["stock"]
        intValue           <- map["intValue"]
        stringValue        <- map["stringValue"]
        weight             <- map["weight"]
        sumSalePercent     <- map["sumSalePercent"]
    }
}
