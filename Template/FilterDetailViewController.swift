
import UIKit


class FilterDetailViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var filtersTableView: UITableView!
    
    
    // MARK: Properties
    var filter: FormattedFilterModel!
    let identifier = "cell"
    
    // MARK: Overriden funcs
    override func viewDidLoad() {
        super.viewDidLoad()
        filter.prepare()
        title = filter.name
        filtersTableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
        filtersTableView.tableFooterView = UIView()
        let closeButton = UIBarButtonItem(title: "Сбросить", style: .plain, target: self, action: #selector(restoreButtonClicked(_:)))
        navigationItem.rightBarButtonItem = closeButton
    }

    
    // MARK: Action funcs
    @IBAction func dismissButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func aplyButtonClicked(_ sender: Any) {
        for f in filter.filter {
            f.checked = f.tempChecked
        }
        dismiss(animated: true, completion: nil)
    }

    @IBAction func restoreButtonClicked(_ sender: Any) {
        for f in filter.filter {
            f.tempChecked = false
        }
        filtersTableView.reloadData()
    }
    
}


extension FilterDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filter.filter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.tintColor = UIColor.colorPrimary
        cell.textLabel?.text = filter.filter[indexPath.row].name
        let filterItem = filter.filter[indexPath.row]
        cell.accessoryType = filterItem.tempChecked ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filterItem = filter.filter[indexPath.row]
        filterItem.tempChecked = !filterItem.tempChecked
        tableView.reloadData()
    }

}
