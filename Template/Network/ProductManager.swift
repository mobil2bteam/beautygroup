
import Alamofire
import ObjectMapper

class ProductManager {
    
    static let appManager = AppManager.shared
    
    class func getProducts(with params: [String: String], successHandler: @escaping (ProductsModel?, String?) -> Void) {
        let r = ServerAPI.products(params: params)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        let model = Mapper<ProductsModel>().map(JSON: JSON!)!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
    
    class func fetchProducts(with params: [String: String], successHandler: @escaping ([CatalogModel]?, String?) -> Void) {
        let r = ServerAPI.getProducts(params: params)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        var products = [CatalogModel]()
                        if let array = JSON?["products"] as? [[String: Any]] {
                            for p in array {
                                let product = Mapper<CatalogModel>().map(JSON: p)!
                                products.append(product)
                            }
                        }
                        successHandler(products, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }

}



