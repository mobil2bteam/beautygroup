
import Foundation
import Alamofire

enum ServerAPI {
    
    case options
    case home
    case settings
    case newsList
    case news(newsId: Int)
    
    /// products
    case product(productId: Int)
    case products(params: [String: String])
    case getProducts(params: [String: String])
    case cart(params: [String: String])
    case catalog(parentID: Int?)
    
    /// profile
    case signUp(params: [String: String])
    case logIn(params: [String: String])
    case updateUser(params: [String: String])
    case changePassword(params: [String: String])
    case restorePassword(email: String)
    
    /// comments
    case getComments
    case addComment(rating: Double, text: String)
    
    /// orders
    case order(params: [String: String])
    case getOrder(orderId: Int)
    case orders
    case updateOrder(orderId: Int, isPayment: Bool)
    
    /// info
    case menu(menuId: Int)
    
    ///
    case partner
}


extension ServerAPI {
    
    var path: String {
        switch self {
        case .options:
            return Constants.apiBaseUrl
        case .home:
            return Constants.apiBaseUrl + "/home"
        case .settings:
            return Constants.apiBaseUrl + "/settings"
        case .catalog:
            return Constants.apiBaseUrl + "/categories"
        case .newsList:
            return Constants.apiBaseUrl + "/news"
        case .products:
            return Constants.apiBaseUrl + "/products"
        case .product:
            return Constants.apiBaseUrl + "/product"
        case .cart:
            return Constants.apiBaseUrl + "/cart/calculate-delivery"
        case .order:
            return Constants.apiBaseUrl + "/order"
        case .news:
            return Constants.apiBaseUrl + "/news/view"
        case .signUp:
            return Constants.apiBaseUrl + "/registration"
        case .logIn:
            return Constants.apiBaseUrl + "/login"
        case .updateUser:
            return Constants.apiBaseUrl + "/user/update"
        case .changePassword:
            return Constants.apiBaseUrl + "/user/change-password"
        case .restorePassword:
            return Constants.apiBaseUrl + "/user/reset"
        case .getComments:
            return Constants.apiBaseUrl + "/review-company/index"
        case .addComment:
            return Constants.apiBaseUrl + "/review-company/create"
        case .orders:
            return Constants.apiBaseUrl + "/user/orders"
        case .getOrder:
            return Constants.apiBaseUrl + "/user/order"
        case .menu:
            return Constants.apiBaseUrl + "/information/view"
        case .partner:
            return Constants.apiBaseUrl + "/feed/create"
        case .updateOrder:
            return Constants.apiBaseUrl + "/order/payment"
        case .getProducts:
            return Constants.apiBaseUrl + "/app/products"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .cart:
            return .post
        case .order:
            return .post
        case .signUp:
            return .post
        case .logIn:
            return .post
        case .updateUser:
            return .post
        case .changePassword:
            return .post
        case .restorePassword:
            return .post
        case .getComments:
            return .post
        case .addComment:
            return .post
        case .orders:
            return .post
        case .getOrder:
            return .post
        case .partner:
            return .post
        case .updateOrder:
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String: Any] {
        // Build default parameters with app key and user token if it exist
        var defaultParameters = ["key": Constants.apiKey]
        if let token = UserDefaults.standard.string(forKey: "token") {
            defaultParameters["token"] = token
        }
        
        switch self {
            
        case .catalog(let parentID):
            if parentID != nil {
                defaultParameters["catalog_id"] = String(parentID!)
            }
            return defaultParameters
            
        case .products(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters
            
        case .cart(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            defaultParameters["city_id"] = "\(CityManager.getDeliveryCity().id)"
            return defaultParameters
            
        case .order(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters
            
        case .getOrder(let orderId):
            defaultParameters["order_id"] = "\(orderId)"
            return defaultParameters
            
        case .updateOrder(let orderId, let isPayment):
            defaultParameters["orderId"] = "\(orderId)"
            defaultParameters["isPayment"] = isPayment == true ? "1" : "0"
            return defaultParameters

        case .signUp(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters
            
        case .logIn(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters

        case .updateUser(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters

        case .changePassword(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters

        case .getProducts(let params):
            for key in params.keys {
                defaultParameters[key] = params[key]
            }
            return defaultParameters

        case .product(let productId):
            defaultParameters["product_id"] = "\(productId)"
            return defaultParameters
            
        case .news(let newsId):
            defaultParameters["news_id"] = "\(newsId)"
            return defaultParameters
            
        case .restorePassword(let email):
            defaultParameters["email"] = email
            return defaultParameters
            
        case .addComment(let rating, let text):
            defaultParameters["text"] = text
            defaultParameters["rating"] = String(rating)
            return defaultParameters
            
        case .menu(let menuId):
            defaultParameters["id"] = "\(menuId)"
            return defaultParameters
            
        default:
            return defaultParameters
        }
    }
}
