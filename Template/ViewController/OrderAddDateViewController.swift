
import UIKit
import FSCalendar

class OrderAddDateViewController: UIViewController {

    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var calendarViewHeightConstraint: NSLayoutConstraint!
    
    var dateHandler: ((Date) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.colorAccent
        title = "Дата доставки"
        prepareCalendarView()
    }

    // MARK: Actions
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonClicked(_ sender: Any) {
        if let date = calendarView.selectedDate {
            dismiss(animated: true, completion: { [weak self] in
                if self?.dateHandler != nil {
                    self?.dateHandler!(date)
                }
            })
        }
    }
    
    // MARK: Methods
    func prepareCalendarView() {
        calendarView.firstWeekday = 2
        calendarView.today = nil
        calendarView.appearance.weekdayTextColor = UIColor.black
        calendarView.appearance.headerTitleColor = UIColor.black
        calendarView.select(Date(), scrollToDate: true)
    }
    
}

extension OrderAddDateViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendarViewHeightConstraint.constant = bounds.size.height
        view.layoutIfNeeded()
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date().addingTimeInterval(60 * 60 * 24 * 30)
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return UIColor.colorAccent
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        if date.compare(calendar.minimumDate) == .orderedAscending || date.compare(calendar.maximumDate) == .orderedDescending {
            return UIColor.colorTextSecond
        }
        return UIColor.black
    }
    
}
