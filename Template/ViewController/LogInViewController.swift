import UIKit
import SVProgressHUD

class LogInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: TextField!
    @IBOutlet weak var passwordTextField: TextField!
    var completionBlock: SignInUserBlock?
    
    // MARK: Actions
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logInButtonClicked(_ sender: Any) {
        view.endEditing(true)
        if emailTextField.text!.isEmpty {
            emailTextField.shake()
            return
        }
        if passwordTextField.text!.isEmpty {
            passwordTextField.shake()
            return
        }
        let params = ["password": passwordTextField.text!,
                      "login": emailTextField.text!]
        SVProgressHUD.show()
        UserManager.login(with: params) { [weak self] (user, error) in
            SVProgressHUD.dismiss()
            if user != nil && self?.completionBlock != nil {
                self?.completionBlock!()
            }
            if error != nil {
                self?.alert(message: error!)
            }
        }
    }
    
    
}

extension LogInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            _ = passwordTextField.becomeFirstResponder()
        }
        _ = textField.resignFirstResponder()
        return true
    }
    
}


