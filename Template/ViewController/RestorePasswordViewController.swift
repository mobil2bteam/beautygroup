
import UIKit
import SVProgressHUD

class RestorePasswordViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var emailTextField: TextField!
    
    
    // MARK: Actions
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func restoreButtonClicked(_ sender: Any) {
        view.endEditing(true)
        if emailTextField.text!.isEmpty {
            emailTextField.shake()
            return
        }
        SVProgressHUD.show()
        UserManager.restorePassword(for: emailTextField.text!) {[weak self] (message, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self?.alert(message: error!)
            }
            if message != nil {
                self?.alert(message: message!, title: "", handler: { [weak self] (action) in
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        }
    }

}

extension RestorePasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = textField.resignFirstResponder()
        return true
    }
}
