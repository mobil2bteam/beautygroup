
import UIKit
import SideMenu

class HamburgerViewController: UIViewController {

    var showMenuButton = false
    var showShoppingButton = true
    var showSearchButton = true

    var cartBarButtonItem: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HamburgerViewController.updateProductCount), name: CartManager.notificationName, object: nil)
        if showMenuButton {
            let menuButton = UIBarButtonItem(image: UIImage.init(named: "List"), style: .plain, target: self, action: #selector(menuButtonClicked))
            navigationItem.leftBarButtonItem = menuButton
        }
        let cartButton = UIBarButtonItem(image: UIImage.init(named: "icon_cart"), style: .plain, target: self, action: #selector(cartButtonClicked))
        cartBarButtonItem = cartButton
        if showSearchButton {
            let searchButton = UIBarButtonItem(image: UIImage.init(named: "icon_search"), style: .plain, target: self, action: #selector(searchButtonClicked))
            navigationItem.rightBarButtonItems = [cartButton, searchButton]
        } else {
            navigationItem.rightBarButtonItem = cartButton
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let number = CartManager.productsCount()
        cartBarButtonItem?.setBadge(text: "\(number)")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: CartManager.notificationName, object: nil)
    }

    // MARK: Actions
    func menuButtonClicked() {
        let vc = UIStoryboard.init(name: "SideMenuStoryboard", bundle: nil).instantiateInitialViewController()
        present(vc!, animated: true, completion: nil)
    }
    
    func cartButtonClicked() {
        let vc = UIStoryboard.init(name: "Cart", bundle: nil).instantiateInitialViewController()
        navigationController?.pushViewController(vc!, animated: true)
    }

    func searchButtonClicked() {
        let vc = UIStoryboard.init(name: "Search", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let searchVC = vc.viewControllers[0] as! SearchViewController
        searchVC.completionClosure = { [weak self] text in
            let params = ["search": text]
            let productsVC = Router.productsViewController(for: params)
            self?.navigationController?.pushViewController(productsVC, animated: true)
        }
        present(vc, animated: true, completion: nil)
    }

    // MARK: Methods
    func updateProductCount() {
        if showShoppingButton {
            let number = CartManager.productsCount()
            cartBarButtonItem?.setBadge(text: "\(number)")
        }
    }

}
