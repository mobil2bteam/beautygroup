
import UIKit
import AlamofireImage
import SVProgressHUD

class PromotionViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var promotionLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var additionalTextLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var additionalTextView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!

    
    // MARK: Properties  
    var newsId: Int!
    var item: NewsItemModel! {
        didSet {
            prepareView()
        }
    }
    
    
    // MARK: Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if item == nil {
            loadNewsItem()
        }
    }
    
    // MARK: Methods
    func loadNewsItem() {
        SVProgressHUD.show()
        OptionsManager.getNewsItem(newsId: newsId) { [weak self] (newsItem, error) in
            SVProgressHUD.dismiss()
            if newsItem != nil {
                self?.item = newsItem
                self?.errorView.isHidden = true
            }
            if error != nil {
                self?.errorLabel.text = error!.localizedDescription
            }
        }
    }

    func prepareView() {
        title = item.name
        promotionLabel.textColor = UIColor.colorAccent
        promotionLabel.text = item.promo ? "Акция" : ""
        if item.addText.characters.count > 0 {
            additionalTextLabel.text = item.addText
        } else {
            additionalTextView.isHidden = true
        }
        videoView.isHidden = item.url.isEmpty
        nameLabel.text = item.name
        dateLabel.text = item.formattedDate()
        descriptionLabel.setHtmlText(item.text)
        itemImageView.image = nil
        if let image = item.imageBig ?? item.image,
            let url =  URL(string: image.url) {
            let multiplier = CGFloat(image.height) / CGFloat(image.width)
            itemImageView.heightAnchor.constraint(equalTo: itemImageView.widthAnchor, multiplier: multiplier).isActive = true
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
            itemImageView.af_setImage(withURL: url)
        }
    }
    
    
    // MARK: Actions
    @IBAction func videoButtonClicked(_ sender: Any) {
        openURL(urlString: item.url)
    }
    

    // MARK: Helper
    private func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
