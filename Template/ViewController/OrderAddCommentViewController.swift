
import UIKit

class OrderAddCommentViewController: UIViewController {
    
    @IBOutlet weak var commentTextView: UITextView!
    
    var commentHandler: ((String) -> ())?
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.colorAccent
        commentTextView.textColor = UIColor.colorTextPrimary
        commentTextView.tintColor = UIColor.colorTextPrimary
        _ = commentTextView.becomeFirstResponder()
        title = "Комментарий к заказу"
    }
    
    
    // MARK: Actions
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonClicked(_ sender: Any) {
        if commentTextView.text!.isEmpty {
            commentTextView.shake()
            return
        }
        let comment = commentTextView.text!
        dismiss(animated: true, completion: { [weak self] in
            if self?.commentHandler != nil {
                self?.commentHandler!(comment)
            }
        })
    }
    
}

