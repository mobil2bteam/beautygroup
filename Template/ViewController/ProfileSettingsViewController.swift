
import UIKit
import SVProgressHUD

class ProfileSettingsViewController: UIViewController {

    @IBOutlet weak var secondNameTextField: TextField!
    @IBOutlet weak var firstNameTextField: TextField!
    @IBOutlet weak var lastNameTextField: TextField!
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    @IBOutlet weak var emailTextField: TextField!
    @IBOutlet weak var phoneTextField: TextField!
    @IBOutlet weak var oldPasswordTextField: TextField!
    @IBOutlet weak var newPasswordTextField: TextField!
    @IBOutlet weak var passwordStackView: UIStackView!
    
    var user: UserModel! {
        get {
            return AppManager.shared.currentUser!
        }
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }

    // MARK: Methods

    func prepareView() {
        secondNameTextField.text = user.info.secondName
        firstNameTextField.text = user.info.firstName
        lastNameTextField.text = user.info.lastName
        emailTextField.text = user.info.email
        phoneTextField.text = user.info.phone
        newPasswordTextField.text = ""
        oldPasswordTextField.text = ""
        passwordStackView.isHidden = user.isSocail
    }
    
    // MARK: Actions
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        view.endEditing(true)
        var params = [String:String]()
        if !firstNameTextField.text!.isEmpty {
            params["first_name"] = firstNameTextField.text!
            params["user_name"] = firstNameTextField.text!
        }
        if !secondNameTextField.text!.isEmpty {
            params["second_name"] = secondNameTextField.text!
        }
        if !lastNameTextField.text!.isEmpty {
            params["last_name"] = lastNameTextField.text!
        }
        if !phoneTextField.text!.isEmpty {
            params["phone"] = phoneTextField.text!
        }
        if !emailTextField.text!.isEmpty {
            params["email"] = emailTextField.text!
            params["login"] = emailTextField.text!
        }
        let gender = genderSegmentControl.selectedSegmentIndex == 0 ? "0" : "1"
        params["gender"] = gender
        SVProgressHUD.show()
        UserManager.updateUser(with: params) { [weak self] (user, error) in
            SVProgressHUD.dismiss()
            if user != nil {
                self?.alert(message: "Данные изменены")
                self?.prepareView()
            }
            if error != nil {
                self?.alert(message: error!)
            }
        }
    }
    
    @IBAction func changePasswordButtonClicked(_ sender: Any) {
        view.endEditing(true)
        if oldPasswordTextField.text!.isEmpty {
            oldPasswordTextField.shake()
            return
        }
        if newPasswordTextField.text!.isEmpty {
            newPasswordTextField.shake()
            return
        }
        let params = ["password_old": oldPasswordTextField.text!,
                      "password_new": newPasswordTextField.text!]
        SVProgressHUD.show()
        UserManager.changePassword(with: params) { [weak self] (user, error) in
            SVProgressHUD.dismiss()
            if user != nil {
                self?.alert(message: "Пароль изменен")
                self?.prepareView()
            }
            if error != nil {
                self?.alert(message: error!)
            }
        }
    }
}

extension ProfileSettingsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == secondNameTextField {
            _ = firstNameTextField.becomeFirstResponder()
        }
        if textField == firstNameTextField {
            _ = lastNameTextField.becomeFirstResponder()
        }
        if textField == emailTextField {
            _ = phoneTextField.becomeFirstResponder()
        }
        if textField == oldPasswordTextField {
            _ = newPasswordTextField.becomeFirstResponder()
        }
        _ = textField.resignFirstResponder()
        return true
    }
    
}
