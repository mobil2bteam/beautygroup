
import UIKit
import SVProgressHUD


class ProductListViewController: HamburgerViewController {
    
    // MARK: Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: PrimaryLinkButton!
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    @IBOutlet weak var filtercollectionView: UICollectionView!
    @IBOutlet weak var itemsCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var filterViewTopSpacingConstraint: NSLayoutConstraint!
    @IBOutlet var buttonsArray: [UIButton]!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: EmptyLabel!
    
    // MARK: Properties
    var params: [String: String]!
    let itemIdentifier = String(describing: ItemCollectionViewCell.self)
    let filterIdentifier = String(describing: FilterCollectionViewCell.self)
    let observationKey = "itemsCollectionView.contentSize"
    let filterSegue = "filterSegue"
    let sortSegue = "sortSegue"
    var products: ProductsModel! {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let subtitle = "Найдено: \(self.products.items.count)"
                self.navigationItem.setTitle(title: self.products.title, subtitle: subtitle)
            }
        }
    }
    
    // MARK: Overriden funcs
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareEmptyView()
        title = ""
        setupCollectionViews()
        sortButton.titleLabel?.numberOfLines = 2
        products == nil ? loadProducts() : prepareView()
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
        _ = buttonsArray.map{ $0.tintColor = UIColor.colorTextPrimary }
    }
    
    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }

    // MARK: Load data
    func loadProducts () {
        if params != nil {
            SVProgressHUD.show()
            ProductManager.getProducts(with: params) { [weak self] (products, error) in
                SVProgressHUD.dismiss()
                if error != nil {
                    self?.errorLabel.text = error!
                }
                if products != nil {
                    self?.products = products
                    self?.prepareView()
                    self?.filtercollectionView.reloadData()
                }
            }
        }
    }

    // MARK: Methods
    func prepareEmptyView() {
        emptyLabel.setHtmlText(Constants.emptyProducts)
    }

    func setupCollectionViews() {
        itemsCollectionView.backgroundColor = UIColor.colorBackgroundDark
        itemsCollectionView.register(ItemCollectionViewCell.nib(), forCellWithReuseIdentifier: itemIdentifier)
        (filtercollectionView.collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize = CGSize(width: 80, height: 30)
        filtercollectionView.register(FilterCollectionViewCell.nib(), forCellWithReuseIdentifier: filterIdentifier)
    }
    
    func showFilterView(show: Bool) {
        filterViewTopSpacingConstraint.constant = show == true ? 12 : -100
        UIView.animate(withDuration: 0.25) { 
            self.view.layoutIfNeeded()
        }
    }
    
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == filterSegue {
            let vc = (segue.destination as! UINavigationController).viewControllers[0] as! FilterViewController
            vc.products = products
            vc.filterBlock = { [weak self] products in
                self?.products = products
                self?.prepareView()
                self?.itemsCollectionView.reloadData()
            }
        }
        if segue.identifier == sortSegue {
            let vc = segue.destination as! SortViewController
            vc.products = products
            vc.sortBlock = { [weak self] products in
                self?.products = products
                self?.prepareView()
            }
        }
    }
    
    // MARK: Notifcation observers
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            let height = itemsCollectionView.collectionViewLayout.collectionViewContentSize.height
            if itemsCollectionViewHeightConstraint.constant != height {
                itemsCollectionViewHeightConstraint.constant = height
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
        }
    }

    
    // MARK: Public funcs
    func prepareView() {
        emptyView.isHidden =  products.items.count != 0
        filtercollectionView.isHidden = products.filter.quickFilters.count == 0
        sortButton.setTitle("Сортировать", for: .normal)
        for s in AppManager.shared.options.directory.sorts {
            if s.id == products.filter.sortId {
                sortButton.setTitle(s.name, for: .normal)
            }
        }
        itemsCollectionView.reloadData()
        filtercollectionView.reloadData()
        errorView.isHidden = true
    }
    
    func showProductViewController(for productId: Int) {
        let vc = Router.productViewController(productId: productId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Navigation
    func showProducts (for params: [String: String]) {
        let vc = Router.productsViewController(for: params)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if products == nil {
            return 0
        }
        if collectionView == itemsCollectionView {
            return products.items.count
        } else {
            return products.filter.quickFilters.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == itemsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemIdentifier, for: indexPath) as! ItemCollectionViewCell
            cell.configure(for: products.items[indexPath.item])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: filterIdentifier, for: indexPath) as! FilterCollectionViewCell
            cell.nameLabel.text = products.filter.quickFilters[indexPath.item].name
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == itemsCollectionView {
            let item = products.items[indexPath.item]
            showProductViewController(for: item.id)
        } else {
            var params = products.filter.filterParams()
            let item = products.filter.quickFilters[indexPath.item]
            if item is BrandModel {
                params["brand_id"] = "\(item.id)"
            } else {
                params["catalog_id"] = "\(item.id)"
                params.removeValue(forKey: "brand_id")
            }
            showProducts(for: params)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == itemsCollectionView {
            let width = collectionView.frame.size.width / 2.0 - 0.5
            return CGSize(width: width, height: ItemCollectionViewCell.itemHeight)
        } else {
            return (collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize
        }
    }

}


extension ProductListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        showFilterView(show: offset >= 100)
        if products?.filter.canLoadMore ?? false {
            if scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height {
                if params != nil {
                    var newParams = params!
                    newParams["page"] = String(products.filter.page! + 1)
                    SVProgressHUD.show()
                    ProductManager.getProducts(with: newParams) { [weak self] (products, error) in
                        SVProgressHUD.dismiss()
                        if error != nil {
                            self?.errorLabel.text = error!
                        }
                        if products != nil {
                            self?.products.items.append(contentsOf: products!.items)
                            self?.products.filter = products!.filter
                            self?.prepareView()
                            self?.filtercollectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
}
