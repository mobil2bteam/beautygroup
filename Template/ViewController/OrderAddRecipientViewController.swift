
import UIKit

class OrderAddRecipientViewController: UIViewController {
    
    @IBOutlet weak var recipientsTableView: UITableView!
    @IBOutlet weak var nameTextField: TextField!
    @IBOutlet weak var phoneTextField: TextField!

    
    var recipientHandler: ((RecipientDataModel) -> ())?
    var recipients: [RecipientDataModel] = [] {
        didSet {
            recipientsTableView.reloadData()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.colorAccent
        view.backgroundColor = UIColor.colorBackgroundDark
        recipients = OrderDataManager.getAllRecipients()
        recipientsTableView.separatorColor = UIColor.colorBackgroundDark
        recipientsTableView.separatorStyle = .singleLine
        recipientsTableView.backgroundColor = UIColor.colorBackgroundDark
        recipientsTableView.tableFooterView = UIView.init()
        title = "Получатель"
    }
    
    
    // MARK: Actions
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonClicked(_ sender: Any) {
        if nameTextField.text!.isEmpty {
            nameTextField.shake()
            return
        }
        if phoneTextField.text!.isEmpty {
            phoneTextField.shake()
            return
        }
        let name = nameTextField.text!
        let phone = phoneTextField.text!
        
        let recipient = OrderDataManager.addRecipient(name: name, phone: phone)
        dismiss(animated: true, completion: { [weak self] in
            if self?.recipientHandler != nil {
                self?.recipientHandler!(recipient)
            }
        })
    }
    
    
    // MARK: Methods
    
    func deleteRecipient(recipient: RecipientDataModel) {
        customAlert(title: "Удалить получателя?") { [weak self] in
            OrderDataManager.removeRecipient(recipient: recipient)
            self?.recipients = OrderDataManager.getAllRecipients()
        }
    }
    
}


extension OrderAddRecipientViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipientTableViewCell", for: indexPath) as! RecipientTableViewCell
        let recipient = recipients[indexPath.row]
        cell.configureRecipient(for: recipient)
        cell.deleteHandler = { [weak self] in
            self?.deleteRecipient(recipient: recipient)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recipient = recipients[indexPath.row]
        dismiss(animated: true, completion: { [weak self] in
            if self?.recipientHandler != nil {
                self?.recipientHandler!(recipient)
            }
        })
    }
}

extension OrderAddRecipientViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            _ = phoneTextField.becomeFirstResponder()
        }
        _ = textField.resignFirstResponder()
        return true
    }
}
