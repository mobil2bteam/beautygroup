
import UIKit
import MessageUI

class InfoViewController: HamburgerViewController {

    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var operatorButton: UIButton!
    @IBOutlet weak var feedBackButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var tableView: UITableView!
    let menus = AppManager.shared.options.settings.menus
    let cellHeight = 50
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
        operatorButton.isHidden = !SettingsManager.showOperatorButton
        feedBackButton.isHidden = !SettingsManager.showFeedbackButton
        textButton.isHidden = !SettingsManager.showWriteUsButton
        tableViewHeightConstraint.constant = CGFloat(cellHeight * menus.count)
        tableView.separatorColor = UIColor.colorBackgroundDark
        view.layoutIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Информация"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
 
    // MARK: Actions
    @IBAction func operatorButtonClicked(_ sender: Any) {
        let onlyDigits = (AppManager.shared.options.settings.company.contact.phone.components(separatedBy: CharacterSet.decimalDigits.inverted)).joined(separator: "")
        let phoneURL = URL.init(string: "telprompt:\(onlyDigits)")
        if phoneURL != nil {
            openURL(urlString: "telprompt:\(onlyDigits)")
        }
    }

    @IBAction func feedBackButtonClicked(_ sender: Any) {
        
    }
    
    @IBAction func writeUsButtonClicked(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            return
        }
        sendEmail()
    }
    
    @IBAction func siteButtonClicked(_ sender: Any) {
        openURL(urlString: "http://www.mobil2b.com")
    }
    
    //MARK: Helper
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        let email = AppManager.shared.options.settings.company.contact.email!
        composeVC.setToRecipients([email])
        self.present(composeVC, animated: true, completion: nil)
    }

    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}

extension InfoViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension InfoViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = menus[indexPath.row].name
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightSemibold)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let menu = menus[indexPath.row]
        let vc = Router.menuInfoViewController(for: menu.id)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
