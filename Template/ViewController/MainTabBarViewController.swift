
import UIKit

class MainTabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    let appManager = AppManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        /*
        let vc1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()!
        vc1.tabBarItem = UITabBarItem.init(title: "Главная", image: nil, tag: 0)
        
        let vc2 = PromotionListViewController()
        let navVC2 = UINavigationController.init(rootViewController: vc2)
        navVC2.tabBarItem = UITabBarItem.init(title: "Акции", image: nil, tag: 1)
        
        let vc3 = UIStoryboard.init(name: "Catalog", bundle: nil).instantiateInitialViewController()!
        vc3.tabBarItem = UITabBarItem.init(title: "Каталог", image: nil, tag: 2)
        
        let vc4 = UIStoryboard.init(name: "Profile", bundle: nil).instantiateInitialViewController()!
        vc4.tabBarItem = UITabBarItem.init(title: "Профиль", image: nil, tag: 3)
        
        let vc5 = InfoViewController.init(nibName: "ContactViewController", bundle: nil)
        let navVC5 = UINavigationController.init(rootViewController: vc5)
        navVC5.tabBarItem = UITabBarItem.init(title: "Инфо", image: nil, tag: 4)
        
        viewControllers = [vc1, navVC2, vc3, vc4, navVC5]
 */
    }

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.tabBarItem.tag == 3 && AppManager.shared.currentUser == nil {
            let vc = UIStoryboard.init(name: "Login", bundle: nil).instantiateInitialViewController()! as! UINavigationController
            (vc.viewControllers[0] as! EnterViewController).completionBlock =  { [weak self] in
                self?.dismiss(animated: true, completion: nil)
                self?.selectedIndex = 3
            }
            present(vc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
}
