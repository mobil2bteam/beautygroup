
import UIKit
import SVProgressHUD

class CatalogViewController: HamburgerViewController {

    var appManager = AppManager.shared
    var catalog: CatalogModel!

    @IBOutlet weak var catalogView: CatalogView!
    @IBOutlet weak var catalogViewHeightConstraint: NSLayoutConstraint!
    
    
    // MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        // если каталог не задан, то берем главный каталог
        if catalog == nil {
            catalog = appManager.catalog.items.filter { $0.id == appManager.options.settings.defaultСatalog }.first
        }
        if catalog != nil {
            prepareView()
            title = catalog.name
        }
    }
    
       
    // MARK: - Preparation view -
    func prepareView() {
        prepareCatalogView()
    }

    func prepareCatalogView() {
        catalogView.delegate = self
        catalogView.configure(for: catalog!)
    }
        
    // MARK: - Navigation
    func showProducts (for catalogId: Int) {
        let vc = Router.productsViewController(for: catalogId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showCatalog (catalog: CatalogModel) {
        let vc = Router.catalogViewController(catalog: catalog)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension CatalogViewController: CatalogViewDelegate {
    
    func catalogView(_ catalogView: CatalogView, didSelectItemAt index: Int) {
        let selectedItem = catalog.items[index]
        if selectedItem.items.count > 0 && !selectedItem.search {
            showCatalog(catalog: selectedItem)
        } else {
            showProducts(for: selectedItem.id)
        }
    }
    
    func didChangeHeight(in catalogItem: CatalogView, height: CGFloat) {
        catalogViewHeightConstraint.constant = height
        DispatchQueue.main.async { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}
