
import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var verificationButton: UIButton!
    @IBOutlet weak var partnerLabel: UILabel!
    @IBOutlet weak var partnerInfoView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var partnerButtonView: UIView!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var partnerButton: UIButton!
    @IBOutlet weak var promoCodeTitleLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var ballLabel: UILabel!
    
    let appManager = AppManager.shared
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = appManager.currentUser {
            userLabel.text = user.info.userName
            emailLabel.text = user.info.email
            ballLabel.text = "Баллы: \(user.balls)"
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(updateUser), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
        verificationButton.tintColor = UIColor.colorAccent
        partnerLabel.backgroundColor = UIColor.colorAccent
        partnerButton.setTitleColor(UIColor.colorTextPrice, for: .normal)
        promoCodeTitleLabel.textColor = UIColor.colorTextPrice
        promoCodeLabel.textColor = UIColor.colorTextPrice
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareView()
    }
    
    @IBAction func exitButtonClicked(_ sender: Any) {
        customAlert(title: "Вы уверены, что хотите выйти?") { [weak self] in
            guard let `self` = self else { return }
            self.appManager.currentUser = nil
            if Constants.appMode == .tabBar {
                self.tabBarController?.selectedIndex = 0
            } else {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    @IBAction func partnerButtonClicked(_ sender: Any) {
        let vc = Router.menuInfoViewController(for: 0)
        vc.isPartnerInfo = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
        let text = AppManager.shared.currentUser?.promoCode?.code ?? ""
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func updateUser() {
        let params = [String: String]()
        UserManager.login(with: params) { (user, error) in
            self.refreshControl.endRefreshing()
            if error != nil {
                self.alert(message: error!)
            }
            if user != nil {
                AppManager.shared.currentUser = user
                self.prepareView()
            }
        }
    }
    
    func prepareView() {
        if let user = appManager.currentUser {
            ballLabel.text = "Баллы: \(user.balls)"
            verificationButton.isHidden = true
            partnerLabel.isHidden = true
            partnerInfoView.isHidden = true
            partnerButtonView.isHidden = true
            infoView.isHidden = true
            if user.info.verify == true {
                verificationButton.isHidden = false
                if user.info.partner == true {
                    partnerLabel.isHidden = false
                    partnerInfoView.isHidden = false
                    verificationButton.isHidden = true
                    if let promo = user.promoCode {
                        promoCodeLabel.text = promo.code
                        saleLabel.text = "Размер скидки по промо-коду: \(promo.percent)%"
                        partnerInfoView.isHidden = false
                    } else {
                        partnerInfoView.isHidden = true
                    }
                } else {
                    partnerButtonView.isHidden = AppManager.shared.options.settings.partnerInfo == nil
                    if user.info.partnerFeed == true {
                        partnerButtonView.isHidden = true
                        infoView.isHidden = false
                        infoLabel.text = "Ваша заявка в обработке. Мы сообщим вам о нашем решении."
                    }
                }
            } else {
                infoView.isHidden = false
                infoLabel.text = "Приложение предназначено только для сертифицированных стилистов, руководителей и владельцев салонов красоты. Нам понадобиться от 24 до 48 часов, чтобы проверить вашу регистрацию. Вы не сможете оформлять заказы до завершения процесса регистрации."
            }
        }
    }

}
