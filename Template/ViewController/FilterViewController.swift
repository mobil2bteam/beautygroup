
import UIKit
import SVProgressHUD

typealias FilterBlock = (ProductsModel) -> ()

class FilterViewController: UIViewController {

    var products: ProductsModel!
    let filterSegue = "detailFilterSegue"
    var currentFilter: FormattedFilterModel!
    let identifier = "cell"
    let observationKey = "filtersTableView.contentSize"
    var filterBlock: FilterBlock?

    @IBOutlet weak var filtersTableView: UITableView!
    @IBOutlet weak var filterTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var saleView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var priceFromTextField: TextField!
    @IBOutlet weak var priceToTextField: TextField!
    @IBOutlet weak var saleSwitch: UISwitch!
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Фильтры"
        prepareView()
        filtersTableView.register(FilterTableViewCell.nib(), forCellReuseIdentifier: identifier)
        filtersTableView.tableFooterView = UIView()
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
    }
    
    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }

    func prepareView() {
        saleView.isHidden = !AppManager.shared.options.settings.filter.price
        priceView.isHidden = !AppManager.shared.options.settings.filter.price
        genderView.isHidden = !AppManager.shared.options.settings.filter.sex
        genderSegmentControl.selectedSegmentIndex = products.filter.sexId
        if products.filter.priceFrom != 0 {
            priceFromTextField.text = "\(products.filter.priceFrom)"
        }
        if products.filter.priceTo != 0 {
            priceToTextField.text = "\(products.filter.priceTo)"
        }
    }
    
    // MARK: Observation
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            let height = filtersTableView.contentSize.height
            filterTableViewHeightConstraint.constant = height
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        filtersTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailFilterSegue" {
            let vc = (segue.destination as! UINavigationController).viewControllers[0] as! FilterDetailViewController
            vc.filter = currentFilter
        }
    }
    
    // MARK: Action funcs
    @IBAction func dismissButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func aplyButtonClicked(_ sender: Any) {
        if AppManager.shared.options.settings.filter.sex {
            products.filter.sexId = genderSegmentControl.selectedSegmentIndex
        }
        if let priceFrom = Int(priceFromTextField.text!) {
            products.filter.priceFrom = priceFrom
        }
        if let priceTo = Int(priceToTextField.text!){
            products.filter.priceTo = priceTo
        }
        SVProgressHUD.show()
        let params = products.filter.filterParams()
        ProductManager.getProducts(with: params) { [weak self] (products, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self?.alert(message: error!)
            }
            if products != nil {
                if self?.filterBlock != nil {
                    self?.filterBlock!(products!)
                }
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func restoreButtonClicked(_ sender: Any) {
        priceFromTextField.text = ""
        priceToTextField.text = ""
        genderSegmentControl.selectedSegmentIndex = 0
        for f in products.filter.formattedFilters(for: AppManager.shared.options) {
            for t in f.filter {
                t.checked = false
                t.tempChecked = false
            }
        }
        filtersTableView.reloadData()
    }

}

extension FilterViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.filter.formattedFilters(for: AppManager.shared.options).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterTableViewCell
        let f = products.filter.formattedFilters(for: AppManager.shared.options)[indexPath.row]
        cell.configure(for: f)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let f = products.filter.formattedFilters(for: AppManager.shared.options)[indexPath.row]
        currentFilter = f
        performSegue(withIdentifier: filterSegue, sender: self)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
