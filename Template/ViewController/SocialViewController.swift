
import UIKit

class SocialViewController: UIViewController {
    
    @IBOutlet weak var socialCollectionView: UICollectionView!
    @IBOutlet weak var socialCollectionViewHeightConstraint: NSLayoutConstraint!
    
    let observationKey = "socialCollectionView.contentSize"
    var source: [[String: String]] = AppManager.shared.options.settings.company.social?.socialSource() ?? []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
    }
    
    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }
    
    // MARK: Observation
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            let height = socialCollectionView.collectionViewLayout.collectionViewContentSize.height
            socialCollectionViewHeightConstraint.constant = height
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }
    
}

extension SocialViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return source.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SocialCollectionViewCell
        let image = UIImage.init(named: source[indexPath.item]["image"]!)
        cell.socialButton.setImage(image, for: .normal)
        cell.url = source[indexPath.item]["url"]!
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalItems = source.count
        let width: CGFloat!
        switch totalItems {
        case 1:
            width = collectionView.frame.size.width
        case 2:
            width = collectionView.frame.size.width / 2.0
        case 4:
            width = collectionView.frame.size.width / 2.0
        default:
            width = collectionView.frame.size.width / 3.0
        }
        return CGSize(width: width, height: 50)
    }
    
}
