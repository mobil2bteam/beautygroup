//
//  RPPaymentVC.h
//  BeautyGroup
//
//  Created by Ruslan on 8/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPPaymentVC : UIViewController

@property (nonatomic, copy) void (^callback)(void);

@property (nonatomic, assign) CGFloat summ;

@property (nonatomic, assign) NSInteger orderId;

@end
