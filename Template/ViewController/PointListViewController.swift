
import UIKit
import GoogleMaps
import CoreLocation

class PointListViewController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var pointsTableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!

    var points: [PointModel]!
    var orderMode = false
    var orderCallback: ((PointModel) -> ())?

    // MARK: Lifecycle
    convenience init(points: [PointModel]) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.points = points
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        getLocationForCity()
        pointsTableView.backgroundColor = UIColor.colorBackgroundDark
        pointsTableView.register(UINib.init(nibName: "PointTableViewCell", bundle: nil), forCellReuseIdentifier: "PointTableViewCell")
        pointsTableView.tableFooterView = UIView.init()
        pointsTableView.separatorColor = UIColor.colorBackgroundDark
        addMarkers()
        let rightBarButton = UIBarButtonItem.init(title: "Список", style: .plain, target: self, action: #selector(rightBarButtonClicked(_:)))
        navigationItem.rightBarButtonItem = rightBarButton
        if orderMode {
            let closeBarButton = UIBarButtonItem.init(image: UIImage.init(named: "icon_close"), style: .plain, target: self, action: #selector(dismiss(_:)))
            navigationItem.leftBarButtonItem = closeBarButton
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Дополнительные адреса"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    @IBAction func rightBarButtonClicked(_ sender: UIBarButtonItem) {
        if mapView.isHidden {
            mapView.isHidden = false
            sender.title = "Список"
        } else {
            mapView.isHidden = true
            sender.title = "Карта"
        }
    }
    
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    func getLocationForCity() {
        if let point = points.first {
            let geocoder = CLGeocoder()
            let address = "Россия \(point.contact.address.city!)"
            geocoder.geocodeAddressString(address) {
                placemarks, error in
                if let placemark = placemarks?.first {
                    let lat = placemark.location?.coordinate.latitude
                    let lon = placemark.location?.coordinate.longitude
                    let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lon!, zoom: 10)
                    self.mapView.camera = camera
                }
            }
        }
    }

    // MARK: Google Maps
    func addMarkers() {
        for p in points {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(p.contact.address.geo.lat!), longitude: CLLocationDegrees(p.contact.address.geo.lng!))
            marker.title = p.name
            marker.snippet = ""
            marker.icon = UIImage.init(named: "geo_point")
            marker.map = mapView
            marker.userData = p
        }
    }
 
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let point = marker.userData as! PointModel
        let vc = PointViewController.init(point: point)
        vc.orderMode = orderMode
        vc.orderCallback = orderCallback
        navigationController?.pushViewController(vc, animated: true)
        return false
    }
    
}

extension PointListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return points.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointTableViewCell", for: indexPath) as! PointTableViewCell
        let point = points[indexPath.row]
        cell.configure(for: point)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let point = points[indexPath.row]
        let vc = PointViewController.init(point: point)
        vc.orderMode = orderMode
        vc.orderCallback = orderCallback
        navigationController?.pushViewController(vc, animated: true)
    }
}
