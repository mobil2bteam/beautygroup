
import UIKit
import ImageSlideshow
import SVProgressHUD
import FTPopOverMenu

class ProductViewController: UIViewController {
    
    var productId: Int!
    var product: ProductModel!
    var selectedPrice: PriceModel! {
        didSet {
            priceLabel.text = "\(selectedPrice!.price) р."
            prepareCartView()
        }
    }
    
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var sliderContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionDetailLabel: MainLabel!
    @IBOutlet weak var descriptionDetailView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var cartStackView: UIStackView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var cartLabel: UILabel!

    
    // MARK: Lifecycle
    convenience init(productId: Int) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.productId = productId
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadProduct()
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        if product != nil {
            title = product.name
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    // MARK: Load data
    func loadProduct() {
        if productId != nil {
            SVProgressHUD.show()
            OptionsManager.getProduct(for: productId) { [weak self] (product, error) in
                SVProgressHUD.dismiss()
                if product != nil {
                    self?.product = product
                    self?.prepareView()
                    self?.errorView.isHidden = true
                }
                if error != nil {
                    self?.errorLabel.text = error!.localizedDescription
                }
            }
        }
    }

    // MARK: Preparation
    func prepareView() {
        title = product.name
        prepareSliderView()
        if product.prices.count > 0 {
            selectedPrice = product.prices.first
        } else {
            cartStackView.isHidden = true
        }
        if !product.longDescription.isEmpty {
            descriptionDetailLabel.setHtmlText(product.longDescription)
            descriptionDetailView.isHidden = false
        } else {
            descriptionDetailView.isHidden = true
        }
        nameLabel.text = product.name
        descriptionLabel.text = product.shortDescription
        
        DataManager.addProductToViewedProducts(product: product)
        if AppManager.shared.currentUser != nil {
            loginView.isHidden = true
            cartStackView.isHidden = product.prices.count == 0
        } else {
            loginView.isHidden = false
            cartStackView.isHidden = true
        }
    }
    
    func prepareCartView() {
        if let product = CartManager.product(for: selectedPrice.id) {
            let summ = product.count * selectedPrice.price
            cartLabel.text = "В корзине \(product.count) шт. на сумму: \(summ) pуб."
        } else {
            cartLabel.text = "В корзине 0 шт. на сумму: 0 pуб."
        }
    }
    
    func prepareSliderView() {
        let images = product.images
        slideshow.prepareFor(images: images, sizeHandler: { [weak self] (multiplier) in
            guard let `self` = self else { return }
            self.slideshow.heightAnchor.constraint(equalTo: self.slideshow.widthAnchor, multiplier: multiplier).isActive = true
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.view.layoutIfNeeded()
            }
        }) {
            self.sliderContainerView.isHidden = true
        }
    }
    
    // MARK: Actions
    @IBAction func cartButtonClicked(_ sender: Any, event: UIEvent) {
        if selectedPrice != nil {
            CartManager.addProduct(for: selectedPrice.id)
            prepareCartView()
        }
    }

}

