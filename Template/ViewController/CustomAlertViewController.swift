
import UIKit

class CustomAlertViewController: UIViewController {

    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!

    var cancelHandler: (() -> ())?
    var okHandler: (() -> ())?
    var message: String! = ""
    var subtitle: String! = ""
    var okButtonTitle: String! = "Да"
    var cancelButtonTitle: String! = "Нет"

    // MARK: Lifecycle
    convenience init(title: String, subtitle: String = "", cancelButtonTitle: String = "Да", okButtonTitle: String = "Нет", cancelHandler: (() -> ())? = nil, okHandler: (() -> ())? = nil) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.message = title
        self.subtitle = subtitle
        self.okButtonTitle = okButtonTitle
        self.cancelButtonTitle = cancelButtonTitle
        self.cancelHandler = cancelHandler
        self.okHandler = okHandler
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = message
        subtitleLabel.text = subtitle
        cancelButton.setTitle(cancelButtonTitle, for: .normal)
        okButton.setTitle(okButtonTitle, for: .normal)
    }
    
    // MARK: Actions
    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true) { [weak self] in
            if self?.cancelHandler != nil {
                self?.cancelHandler!()
            }
        }
    }
    
    @IBAction func okButtonClicked(_ sender: Any) {
        dismiss(animated: true) { [weak self] in
            if self?.okHandler != nil {
                self?.okHandler!()
            }
        }
    }

    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
