

import UIKit

class GroupViewController: UIViewController {

    var groups: [GroupModel] = []
    let identifier = String(describing: GroupTableViewCell.self)
    let cellsInRow: CGFloat = 2.0
    @IBOutlet var groupsTableView: UITableView!
    @IBOutlet var groupsTableViewHeightConstraint: NSLayoutConstraint!
    let observationKey = "groupsTableView.contentSize"
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        setup()
    }

    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }
    
    
    // MARK: Methods
    private func setup() {
        groupsTableView.register(GroupTableViewCell.nib(), forCellReuseIdentifier: identifier)
        groupsTableView.delegate = self
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
    }
    
    func showProductViewController(for productId: Int) {
        let vc = Router.productViewController(productId: productId)
        navigationController?.pushViewController(vc, animated: true)
    }

    
    // MARK: Observation
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            let height = groupsTableView.contentSize.height
            groupsTableViewHeightConstraint.constant = height
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }
    
}

extension GroupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! GroupTableViewCell
        cell.configure(for: groups[indexPath.row])
        cell.clickClosure = { [weak self] item in
            self?.showProductViewController(for: item.id)
        }
        cell.moreClosure = { [weak self] group in
            if let link = group.link {
                if self != nil {
                    OptionsManager.request(for: link, fromController: self!)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let group = groups[indexPath.row]
        if group.scrollDirection == .vertical {
            var rowsCount = group.items.count / Int(group.itemsDisplay)
            if group.items.count % Int(group.itemsDisplay) != 0 {
                rowsCount = rowsCount + 1
            }
            return CGFloat(rowsCount) * ItemCollectionViewCell.itemHeight + 50
        }
        return ItemCollectionViewCell.itemHeight + 50
    }

}

