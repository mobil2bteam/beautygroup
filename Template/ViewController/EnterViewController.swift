
import UIKit
import VK_ios_sdk
import FacebookLogin
import FBSDKLoginKit
import SVProgressHUD

class EnterViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var recentViewedView: UIView!
    
    // MARK: Properties
    var completionBlock: SignInUserBlock?
    let signUpSegue = "signUpSegue"
    let logInSegue = "logInSegue"
    

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
        recentViewedView.isHidden = !SettingsManager.showWatchedProducts
        let vkInstance = VKSdk.initialize(withAppId: Constants.vkIdentifier)
        vkInstance?.register(self)
        vkInstance?.uiDelegate = self
    }
    
    // MARK: Actions
    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func vkButtonClicked(_ sender: Any) {
        view.endEditing(true)
        VKSdk.authorize([VK_PER_WALL, VK_PER_EMAIL])
    }
    
    @IBAction func fbButtonClicked(_ sender: Any) {
        view.endEditing(true)
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(_, _, _ ):
                self.getFBUserData()
            }
        }
    }
    
    func socialLogin(_ params: [String: String]) {
        SVProgressHUD.show()
        UserManager.login(with: params) { [weak self] (user, error) in
            SVProgressHUD.dismiss()
            if user != nil && self?.completionBlock != nil {
                self?.completionBlock!()
            }
            if error != nil {
                let vc = Router.signUpViewController()
                vc.completionBlock = self?.completionBlock
                vc.socialSignUp = true
                vc.socialParams = params
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    private func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    let userId = dict["id"] as! String
                    let email = dict["email"] as? String ?? ""
                    var name = dict["name"] as? String ?? ""
                    var lastName = ""
                    var names = name.components(separatedBy:" ")
                    if names.count > 1 {
                        name = names.first!
                        lastName = names[1]
                    }
                    let password = userId.md5()
                    let params = ["login":userId,
                                  "email": email,
                                  "name": name,
                                  "lastName":lastName,
                                  "type": "facebook",
                                  "network": "1",
                                  "password":password]
                    self.socialLogin(params)
                } else {
                    self.alert(message: "Произошла ошибка")
                }
            })
        }
    }

    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == signUpSegue {
            let destination = (segue.destination as! UINavigationController).viewControllers[0] as! SignUpViewController
            destination.completionBlock = completionBlock
        }
        if segue.identifier == logInSegue {
            let destination = (segue.destination as! UINavigationController).viewControllers[0] as! LogInViewController
            destination.completionBlock = completionBlock
        }
    }
}

extension EnterViewController: VKSdkUIDelegate, VKSdkDelegate {
    
    public func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        
        guard (result.token?.accessToken) != nil else {
            return
        }
        if result != nil {
            let userId = result!.token!.userId!
            let email = result!.token.email!
            let name = result!.user?.first_name ?? ""
            let lastName = result!.user?.last_name ?? ""
            let password = userId.md5()
            let params = ["login":userId,
                          "email": email,
                          "name": name,
                          "lastName":lastName,
                          "type": "vk",
                          "network": "1",
                          "password":password]
            self.socialLogin(params)
        }
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        let vc = VKCaptchaViewController.captchaControllerWithError(captchaError)!
        present(vc, animated: true, completion: nil)
    }
    
    func vkSdkUserAuthorizationFailed() {
    }
}

