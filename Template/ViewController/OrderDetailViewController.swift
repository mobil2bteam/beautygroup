

import UIKit

class OrderDetailViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressCommentLabel: UILabel!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var clearAddressButton: UIButton!
    @IBOutlet weak var addressContentView: UIView!
    
    @IBOutlet weak var clearRecipientButton: UIButton!
    @IBOutlet weak var recipientView: UIView!
    @IBOutlet weak var recipientName: UILabel!
    @IBOutlet weak var recipientPhoneLabel: UILabel!
    @IBOutlet weak var recipientContentView: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var clearDateButton: UIButton!
    @IBOutlet weak var dateContentView: UIView!

    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var clearCommentButton: UIButton!
    @IBOutlet weak var commentContentView: UIView!

    
    // MARK: Properties
    var delivery: DeliveryModel!
    var params: [String: String]!

    var address: AddressDataModel? {
        didSet {
            prepareAddressView()
        }
    }
    
    var recipient: RecipientDataModel? {
        didSet {
            prepareRecipientView()
        }
    }
    
    var comment: String = "" {
        didSet {
            prepareCommentView()
        }
    }
    
    var date: Date? {
        didSet {
            prepareDateView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Новый заказ"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    // MARK: Actions
    @IBAction func clearOrderButtonClicked(_ sender: Any) {
        customAlert(title: "Очистить?") { [weak self] in
            self?.address = nil
            if SettingsManager.showRecipientView {
                self?.recipient = nil
            }
            if SettingsManager.showDateView {
                self?.date = nil
            }
            if SettingsManager.showCommentView {
                self?.comment = ""
            }
        }
    }

    @IBAction func clearAddressButtonClicked(_ sender: Any) {
        customAlert(title: "Очистить?") { [weak self] in
            self?.address = nil
        }
    }
    
    @IBAction func clearRecipientButtonClicked(_ sender: Any) {
        customAlert(title: "Очистить?") { [weak self] in
            self?.recipient = nil
        }
    }

    @IBAction func clearDateButtonClicked(_ sender: Any) {
        customAlert(title: "Очистить?") { [weak self] in
            self?.date = nil
        }
    }

    @IBAction func clearCommentButtonClicked(_ sender: Any) {
        customAlert(title: "Очистить?") { [weak self] in
            self?.comment = ""
        }
    }

    @IBAction func addRecipientButtonClicked(_ sender: Any) {
        if recipient != nil {
            return
        }
        let vc = Router.orderAddRecipientViewController()
        vc.recipientHandler = { [weak self] recipient in
            self?.recipient = recipient
        }
        let navVC = UINavigationController.init(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }
    
    @IBAction func addAddressButtonClicked(_ sender: Any) {
        if address != nil {
            return
        }
        let vc = Router.orderAddAddressViewController()
        vc.addressHandler = { [weak self] address in
            self?.address = address
        }
        let navVC = UINavigationController.init(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }
    
    @IBAction func addCommentButtonClicked(_ sender: Any) {
        if !comment.isEmpty {
            return
        }
        let vc = Router.orderAddCommentViewController()
        vc.commentHandler = { [weak self] comment in
            self?.comment = comment
        }
        let navVC = UINavigationController.init(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }
    
    @IBAction func addDateButtonClicked(_ sender: Any) {
        if date != nil {
            return
        }
        let vc = Router.orderAddDateViewController()
        vc.dateHandler = { [weak self] date in
            self?.date = date
        }
        let navVC = UINavigationController.init(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }

    @IBAction func continueButtonClicked(_ sender: Any) {
        if address == nil {
            addressContentView.shake()
            return
        }
        if SettingsManager.showRecipientView && SettingsManager.recipientIsRequired {
            if recipient == nil {
                recipientContentView.shake()
                return
            }
        }
        if SettingsManager.showDateView && SettingsManager.dateIsRequired {
            if date == nil {
                dateContentView.shake()
                return
            }
        }
        if SettingsManager.showCommentView && SettingsManager.commentIsRequired {
            if comment.isEmpty {
                commentContentView.shake()
                return
            }
        }
        OrderDataManager.setLastOrderWithRecipient(withRecipient: recipient != nil)
        if AppManager.shared.currentUser != nil {
            submitOrder()
        } else {
            let vc = Router.signUpViewController()
            vc.completionBlock = { [weak self] in
                self?.dismiss(animated: false, completion: nil)
                self?.submitOrder()
            }
            let navVC = UINavigationController.init(rootViewController: vc)
            present(navVC, animated: true, completion: nil)
        }
    }
    
    // MARK: Methods
    func submitOrder() {
        let vc = Router.orderSubmitViewController()
        vc.delivery = delivery
        vc.params = params
        vc.address = address
        vc.comment = comment
        vc.recipient = recipient
        vc.date = date
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func prepareView() {
        recipientContentView.isHidden = !SettingsManager.showRecipientView
        commentContentView.isHidden = !SettingsManager.showCommentView
        dateContentView.isHidden = !SettingsManager.showDateView
        address = OrderDataManager.getAllAdresses().first
        if OrderDataManager.showRecipientIfPossible() {
            recipient = OrderDataManager.getAllRecipients().first
        } else {
            recipient = nil
        }
        comment = ""
        date = nil
    }
    
    func prepareAddressView() {
        if address != nil {
            clearAddressButton.isHidden = false
            addressLabel.text = address!.formattedAddress()
            addressCommentLabel.text = address!.comment
            addressView.isHidden = false
        } else {
            clearAddressButton.isHidden = true
            addressView.isHidden = true
        }
    }
    
    func prepareCommentView() {
        commentLabel.text = comment
        clearCommentButton.isHidden = comment.isEmpty
    }
    
    func prepareRecipientView() {
        if recipient != nil {
            clearRecipientButton.isHidden = false
            recipientPhoneLabel.text = recipient!.phone
            recipientName.text = recipient!.name
            recipientView.isHidden = false
        } else {
            clearRecipientButton.isHidden = true
            recipientView.isHidden = true
        }
    }
    
    func prepareDateView() {
        if date != nil {
            let dayNameFormatter = DateFormatter(withFormat: "d MMMM y (EEEE)", locale: "ru_RU")
            dateLabel.text = dayNameFormatter.string(from: date!)
        } else {
            dateLabel.text = ""
        }
        clearDateButton.isHidden = date == nil
    }
    
}
