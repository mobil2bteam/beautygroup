//
//  RPPaymentVC.m
//  BeautyGroup
//
//  Created by Ruslan on 8/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
// token - 477BBA133C182267FE5F086924ABDC5DB71F77BFC27F01F2843F2CDC69D89F05

#import "RPPaymentVC.h"
#import <CloudPaymentsAPI/CPService.h>
#import "AFNetworking.h"
#import "AFHTTPSessionOperation.h"
#import "UIViewController+Alert.h"

@interface RPPaymentVC ()

@property (nonatomic, strong) CPService *apiService;
// These values you MUST store at your server.
@property (nonatomic, strong) NSString *apiPublicID;

@property (nonatomic, strong) NSString *apiSecret;

@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *cardOwnerTextField;
@property (weak, nonatomic) IBOutlet UITextField *cardExpirationDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *cardCVVTextField;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (strong, nonatomic) UIWebView *webView;
@end

@implementation RPPaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.sumLabel.text = [NSString stringWithFormat:@"%.2f RUB", self.summ];
    self.orderLabel.text = [NSString stringWithFormat:@"Оплата заказа №%ld", (long)self.orderId];
    _apiService = [[CPService alloc] init];
    _apiPublicID = @"pk_e9e708048853e7d68b5ba0de60417";
    _apiSecret = @"e6321adb18f164f8e63bd4a37b4895fb";
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:self action:@selector(closeAction:)];
    self.navigationItem.leftBarButtonItem = closeButton;
}

- (IBAction)closeAction:(id)sender {
    if (self.webView != nil) {
        [self.webView removeFromSuperview];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)makePaymentAction:(id)sender {
    NSString *cardNumberString = self.cardNumberTextField.text;
    if (![CPService isCardNumberValid:cardNumberString]) {
        [self showMessage:@"Введите корректный номер карты" withTitle:nil];
        return;
    }
    
    // ExpDate must be in YYMM format
    NSArray *cardDateComponents = [self.cardExpirationDateTextField.text componentsSeparatedByString:@"/"];
    NSString *cardExpirationDateString = [NSString stringWithFormat:@"%@%@",cardDateComponents[1],cardDateComponents[0]];
    
    // create dictionary with parameters for send
    NSMutableDictionary *paramsDictionary = [[NSMutableDictionary alloc] init];
    
    NSString *cryptogramPacket = [_apiService makeCardCryptogramPacket:self.cardNumberTextField.text
                                                            andExpDate:cardExpirationDateString
                                                                andCVV:self.cardCVVTextField.text
                                                      andStorePublicID:_apiPublicID];
    
    [paramsDictionary setObject:cryptogramPacket forKey:@"CardCryptogramPacket"];
    [paramsDictionary setObject:@(self.summ) forKey:@"Amount"];
    [paramsDictionary setObject:@"RUB" forKey:@"Currency"];
    [paramsDictionary setObject:self.cardOwnerTextField.text forKey:@"Name"];
    paramsDictionary[@"InvoiceId"] = @(self.orderId);
    
    NSString *apiURLString = @"https://api.cloudpayments.ru/payments/cards/charge";
    
    // setup AFHTTPRequestOperationManager HTTP BasicAuth and serializers
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:_apiPublicID password:_apiSecret];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [self addMBProgressHUD];
    [manager POST:apiURLString parameters:paramsDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideMBProgressHUD];
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                        BOOL isSuccess = [[responseObject objectForKey:@"Success"] boolValue];
                        if (isSuccess) {
                            [self showMessage:@"Оплата прошла успешно" withOkHandler:^(UIAlertAction *action) {
                                [self successfulPayment];
                            }];
                        } else {
                            NSDictionary *model = [responseObject objectForKey:@"Model"];
                            if (([responseObject objectForKey:@"Message"]) && ![[responseObject objectForKey:@"Message"] isKindOfClass:[NSNull class]]) {
                                // some error
                                [self showMessage:[responseObject objectForKey:@"Message"] withTitle:nil];
                            } else if (([model objectForKey:@"CardHolderMessage"]) && ![[model objectForKey:@"CardHolderMessage"] isKindOfClass:[NSNull class]]) {
                                // some error from acquier
                                [self showMessage:[model objectForKey:@"CardHolderMessage"] withTitle:nil];
                            } else if (([model objectForKey:@"AcsUrl"]) && ![[model objectForKey:@"AcsUrl"] isKindOfClass:[NSNull class]]) {
                                // need for 3DSecure request
                                [self make3DSPaymentWithAcsURLString:(NSString *) [model objectForKey:@"AcsUrl"] andPaReqString:(NSString *) [model objectForKey:@"PaReq"] andTransactionIdString:[[model objectForKey:@"TransactionId"] stringValue]];
                            }
                        }
                    }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideMBProgressHUD];
        [self showMessage:error.localizedDescription withTitle:@"Ошибка!"];
    }];
}

-(void) complete3DSPaymentWithPaResString: (NSString *) paResString andTransactionIdString: (NSString *) transactionIdString {
    
    // create dictionary with parameters for send
    NSMutableDictionary *paramsDictionary = [[NSMutableDictionary alloc] init];
    
    [paramsDictionary setObject:paResString forKey:@"PaRes"];
    [paramsDictionary setObject:transactionIdString forKey:@"TransactionId"];
    
    NSString *apiURLString = @"https://api.cloudpayments.ru/payments/mobile/cards/post3ds";

    // setup AFHTTPRequestOperationManager HTTP BasicAuth and serializers
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:_apiPublicID password:_apiSecret];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [self addMBProgressHUD];
    [manager POST:apiURLString parameters:paramsDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideMBProgressHUD];
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            BOOL isSuccess = [[responseObject objectForKey:@"Success"] boolValue];
            if (isSuccess) {
                NSDictionary *model = [responseObject objectForKey:@"Model"];
                if (([model objectForKey:@"CardHolderMessage"]) && ![[model objectForKey:@"CardHolderMessage"] isKindOfClass:[NSNull class]]) {
                    // some error from acquier
                    [self showMessage:[model objectForKey:@"CardHolderMessage"] withOkHandler:^(UIAlertAction *action) {
                        [self successfulPayment];
                    }];
                } else {
                    [self showMessage:@"Оплата прошла успешно" withOkHandler:^(UIAlertAction *action) {
                        [self successfulPayment];
                    }];
                }
            } else {
                NSDictionary *model = [responseObject objectForKey:@"Model"];
                if (([responseObject objectForKey:@"Message"]) && ![[responseObject objectForKey:@"Message"] isKindOfClass:[NSNull class]]) {
                    // some error
                    [self showMessage:[responseObject objectForKey:@"Message"] withTitle:nil];

                } else if (([model objectForKey:@"CardHolderMessage"]) && ![[model objectForKey:@"CardHolderMessage"] isKindOfClass:[NSNull class]]) {
                    // some error from acquier
                    [self showMessage:[model objectForKey:@"CardHolderMessage"] withTitle:nil];
                } else {
                    [self showMessage:@"Error" withTitle:nil];
                }
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideMBProgressHUD];
        [self showMessage:error.localizedDescription withTitle:@"Ошибка!"];
    }];
    
}


-(void) make3DSPaymentWithAcsURLString: (NSString *) acsUrlString andPaReqString: (NSString *) paReqString andTransactionIdString: (NSString *) transactionIdString {
    
    NSDictionary *postParameters = @{@"MD": transactionIdString, @"TermUrl": @"http://cloudpayments.ru/", @"PaReq": paReqString};
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST"
                                                                                 URLString:acsUrlString
                                                                                parameters:postParameters
                                                                                     error:nil];
    
    [request setValue:@"ru;q=1, en;q=0.9" forHTTPHeaderField:@"Accept-Language"];
    
    NSHTTPURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    
    if ([response statusCode] == 200 || [response statusCode] == 201) {
        self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
        self.webView.delegate = self;
        [self.view addSubview:self.webView];
        
        [self.webView loadData:responseData
                 MIMEType:[response MIMEType]
         textEncodingName:[response textEncodingName]
                  baseURL:[response URL]];
    } else {
        NSString *messageString = [NSString stringWithFormat:@"Unable to load 3DS autorization page.\nStatus code: %d", (unsigned int)[response statusCode]];
        [self showMessage:messageString withTitle:@"Ошибка!"];

    }
}

#pragma mark - UIWebViewDelegate implementation

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *urlString = [request.URL absoluteString];
    if ([urlString isEqualToString:@"http://cloudpayments.ru/"]) {
        NSString *response = [[NSString alloc] initWithData:request.HTTPBody encoding:NSASCIIStringEncoding];
        
        NSDictionary *responseDictionary = [self parseQueryString:response];
        [self.webView removeFromSuperview];
        
        [self complete3DSPaymentWithPaResString:[responseDictionary objectForKey:@"PaRes"] andTransactionIdString:[responseDictionary objectForKey:@"MD"]];
        return NO;
    }
    
    return YES;
}

#pragma mark - Utilities
- (NSDictionary *)parseQueryString:(NSString *)query {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [dict setObject:val forKey:key];
    }
    return dict;
}

- (void)successfulPayment{
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.callback) {
            self.callback();
        }
    }];
}

#pragma mark - UITextFieldDelegate implementation
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    // check if card number valid
    if ([textField isEqual:self.cardNumberTextField]) {
        NSString *cardNumberString = textField.text;
        if ([CPService isCardNumberValid:cardNumberString]) {
            [textField resignFirstResponder];
            return YES;
        } else {
            [self showMessage:@"Введите корректный номер карты" withTitle:nil];
            return NO;
        }
    }
    
    // check if valid length of expiration date
    if ([textField isEqual:self.cardExpirationDateTextField]) {
        NSString *cardExpirationDateString = textField.text;
        if (cardExpirationDateString.length < 5) {
            [self showMessage:@"Введите 4 цифры даты окончания действия карты в формате MM/YY" withTitle:nil];
            return NO;
        }
        
        NSArray *dateComponents = [textField.text componentsSeparatedByString:@"/"];
        if(dateComponents.count == 2) {
            NSDate *date = [NSDate date];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
            NSInteger currentMonth = [components month];
            NSInteger currentYear = [[[NSString stringWithFormat:@"%ld",(long)[components year]] substringFromIndex:2] integerValue];
            
            if([dateComponents[1] intValue] < currentYear) {
                [self showMessage:@"Карта недействительна." withTitle:nil];
                [textField becomeFirstResponder];
                return NO;
            }
            
            if (([dateComponents[0] intValue] > 12)) {
                [self showMessage:@"Карта недействительна." withTitle:nil];
                [textField becomeFirstResponder];
                return NO;
            }
            
            if([dateComponents[0] integerValue] < currentMonth && [dateComponents[1] intValue] <= currentYear) {
                [self showMessage:@"Карта недействительна." withTitle:nil];
                [textField becomeFirstResponder];
                return NO;
            }
        }
        
        [textField resignFirstResponder];
        return YES;
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:self.cardExpirationDateTextField]) {
        
        // handle backspace
        if (range.length > 0 && [string length] == 0) {
            return YES;
        }
        
        if (textField.text.length >= 5) {
            return NO;
        }
        
        NSString *addChar = [[string componentsSeparatedByCharactersInSet:
                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                             componentsJoinedByString:@""];
        
        switch (textField.text.length) {
            case 0:
            case 3:
            case 4:
                textField.text = [textField.text stringByAppendingString:addChar];
                break;
            case 1:
                textField.text = [textField.text stringByAppendingString:addChar];
                textField.text = [textField.text stringByAppendingString:@"/"];
                break;
            default:
                break;
        }
        
        return NO;
    }
    
    return YES;
}


@end
