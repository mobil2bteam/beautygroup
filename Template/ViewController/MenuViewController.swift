
import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet var menuHeightConstraint: NSLayoutConstraint!
    @IBOutlet var menuCollectionView: UICollectionView!

    var menu: MenuModel = MenuModel()
    let observationKey = "menuCollectionView.contentSize"

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        setup()
    }
    
    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }
    
    // MARK: Setup
    
    private func setup() {
        (menuCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = menu.style.direction
        registerMenuCells()
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
    }
    
    private func registerMenuCells() {
        menuCollectionView.register(MenuCollectionViewCellText.nib(), forCellWithReuseIdentifier: MenuStyle.text.rawValue)
        menuCollectionView.register(MenuCollectionViewCellVerticalIcon.nib(), forCellWithReuseIdentifier: MenuStyle.verticalIcon.rawValue)
        menuCollectionView.register(MenuCollectionViewCellVerticalImage.nib(), forCellWithReuseIdentifier: MenuStyle.verticalImage.rawValue)
        menuCollectionView.register(MenuCollectionViewCellHorizontalIcon.nib(), forCellWithReuseIdentifier: MenuStyle.horizontalIcon.rawValue)
        menuCollectionView.register(MenuCollectionViewCellHorizontalImage.nib(), forCellWithReuseIdentifier: MenuStyle.horizontalImage.rawValue)
        menuCollectionView.register(MenuCollectionViewCellHorizontalIcon.nib(), forCellWithReuseIdentifier: MenuStyle.cellIcon.rawValue)
        menuCollectionView.register(MenuCollectionViewCellHorizontalImage.nib(), forCellWithReuseIdentifier: MenuStyle.cellImage.rawValue)
    }
    
    // MARK: Methods

    
    // MARK: Observation
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            var height = menuCollectionView.collectionViewLayout.collectionViewContentSize.height
            if menu.style.direction == .horizontal {
                height = menu.style.height
            }
            menuHeightConstraint.constant = height
            DispatchQueue.main.async { [weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }

}


extension MenuViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let link = menu.items[indexPath.item].link {
            OptionsManager.request(for: link, fromController: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menu.style.rawValue, for: indexPath) as! MenuCollectionViewCell
        cell.configure(for: menu.items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsInRow = menu.style.cellsInRow
        if menu.style == .verticalImage || menu.style == .cellIcon || menu.style == .cellImage {
            let totalItems = menu.items.count
            let currentItem = indexPath.item + 1
            if totalItems % Int(cellsInRow) != 0 && currentItem > Int(cellsInRow) {
                if (currentItem > totalItems - (totalItems % Int(cellsInRow)) || currentItem == totalItems) {
                    let oldWidth = menu.style.cellSize(for: collectionView.frame.width).width
                    let height = menu.style.cellSize(for: collectionView.frame.width).height
                    let itemsInLastRow = Int(totalItems) % Int(cellsInRow)
                    let newWidth = (oldWidth * cellsInRow) / CGFloat(itemsInLastRow)
                    return CGSize(width: floor(newWidth), height: height)
                }
            }
        }
        return menu.style.cellSize(for: collectionView.frame.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
