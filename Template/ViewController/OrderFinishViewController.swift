
import UIKit
import SVProgressHUD

class OrderFinishViewController: UIViewController {

    var order: OrderModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.colorPrimary
        CartManager.clearCart()
        if order?.payment != nil {
            if !order!.isPayment && order.payment!.paymentType == .cart {
                    let vc = RPPaymentVC.init(nibName: "RPPaymentVC", bundle: nil)
                    vc.orderId = order.id
                    vc.summ = CGFloat(order.total)
                    vc.callback = {
                        self.updateOder()
                    }
                    let navVC = UINavigationController.init(rootViewController: vc)
                present(navVC, animated: false, completion: nil)
            }
        }
    }


    @IBAction func okButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateOder() {
        SVProgressHUD.show()
        OptionsManager.updateOrder(for: order.id, isPayment: true) { (order, error) in
            SVProgressHUD.dismiss()
            if order != nil {
                if order!.isPayment {
                    self.alert(message: "Заказ оплачен")
                }
            }
            if error != nil {
                self.alert(message: "Произошла ошибка", title: "", handler: { action in
                    self.updateOder()
                })
            }
        }
    }
}
