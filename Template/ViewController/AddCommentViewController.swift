
import UIKit
import Cosmos

class AddCommentViewController: UIViewController {

    @IBOutlet weak var closeButton: AccentLinkButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    var commentClosure: ((String, Double) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        closeButton.backgroundColor = UIColor.clear
        commentLabel.textColor = UIColor.colorTextSecond
        ratingView.settings.filledColor = UIColor.colorAccent
        ratingView.settings.emptyColor = UIColor.colorBackgroundDark
        ratingView.settings.filledBorderColor = UIColor.colorAccent
    }

    // MARK: Actions
    @IBAction func addCommentButtonClicke(_ sender: Any) {
        dismiss(animated: true) { [weak self] in
            if self?.commentClosure != nil {
                self?.commentClosure!((self?.commentTextView.text)!, (self?.ratingView.rating)!)
            }
        }
    }
    
    @IBAction func dismissButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
