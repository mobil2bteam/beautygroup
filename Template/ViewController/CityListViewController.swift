
import UIKit

class CityListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let identifier = String(describing: CityTableViewCell.self)
    let searchController = UISearchController(searchResultsController: nil)
    var options: OptionsModel! = AppManager.shared.options

    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Регион"
        setupTableView()
        setupSearchController()
        let closeButton = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(dissmiss))
        navigationItem.leftBarButtonItem = closeButton
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView.init()
        tableView.register(CityTableViewCell.nib(), forCellReuseIdentifier: identifier)
    }
    
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск города"
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
    }
    
    // MARK: Actions
    
    func dissmiss() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Helper
    
    func cityArray() -> [CityModel] {
        let searchText = searchController.searchBar.text
        if (searchText!.characters.count > 0) {
            return options.directory.cities.filter {
                $0.name.range(of: searchText!, options: .caseInsensitive) != nil
            }
        } else {
           return options.directory.cities
        }
    }
}

extension CityListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityArray().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CityTableViewCell
        let city = cityArray()[indexPath.row]
        cell.configure(for: city)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = cityArray()[indexPath.row]
        CityManager.setDeliveryCity(city: city)
        dismiss(animated: true, completion: nil)
    }
}

extension CityListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        tableView.reloadData()
    }
}
