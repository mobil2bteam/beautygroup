
import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import FTPopOverMenu
import Fabric
import Crashlytics
import FBSDKCoreKit
import VK_ios_sdk
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        /// clear token from previous version 
        if !UserDefaults.standard.bool(forKey: "newVersion") {
            UserDefaults.standard.removeObject(forKey: "token")
            UserDefaults.standard.set(true, forKey: "newVersion")
            UserDefaults.standard.synchronize()
        }
        ///
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        IQKeyboardManager.sharedManager().enable = true
        GMSServices.provideAPIKey("AIzaSyAkN1ZAqXBNhtLR5p6T3hEeAaVvvUX9dO8")
        UIApplication.shared.statusBarStyle = .default
        StyleManager.shared.styleAppearance()
        CacheManager.updateCache()
        FTPopOverMenuConfiguration.default().textFont = UIFont.systemFont(ofSize: 14)
        FTPopOverMenuConfiguration.default().menuWidth = 140
        Fabric.with([Crashlytics.self])
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        VKSdk.processOpen(url, fromApplication: sourceApplication)
        FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//        Messaging.messaging().apnsToken = fcmToken
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let body = userInfo["aps"] as? [String: Any], let alert = body["alert"] as? String {
            window?.rootViewController?.alert(message: alert)
        }
        completionHandler(.newData)
    }
}

