
import ImageSlideshow

extension ImageSlideshow {
    
    func prepareFor(images: [ImageModel], sizeHandler: ((CGFloat) -> ())? = nil, errorHandler: (() -> ())? = nil) {
        let correctedImages = images.filter{ $0.isCorrect == true }
        if correctedImages.count > 0 {
            let image = correctedImages[0]
            sizeHandler?(image.aspectRatio)
            draggingEnabled = true
            prepareSlider(for: correctedImages)
        } else {
            errorHandler?()
        }
    }
    
    func prepareFor(slider: SliderModel, sizeHandler: ((CGFloat) -> ())? = nil, errorHandler: (() -> ())? = nil) {
        let correctedImages = slider.correctedImages
        if correctedImages.count > 0 {
            if slider.auto {
                slideshowInterval = Double(slider.speed / 1000)
            } else {
                slideshowInterval = 0
            }
            let image = correctedImages[0]
            sizeHandler?(image.aspectRatio)
            draggingEnabled = slider.manual
            prepareSlider(for: correctedImages)
        } else {
            errorHandler?()
        }
    }
    
    private func prepareSlider(for images: [ImageModel]) {
        pageControlPosition = images.count > 1 ? PageControlPosition.insideScrollView : PageControlPosition.hidden
        pageControl.currentPageIndicatorTintColor = UIColor.colorAccent
        pageControl.pageIndicatorTintColor = UIColor.white
        contentScaleMode = UIViewContentMode.scaleAspectFit
        setImageInputs(images.map{ AlamofireSource(urlString: $0.url)! })
    }

}
