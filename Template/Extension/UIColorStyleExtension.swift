
import UIKit

extension UIColor {
    
    // Основной цвет приложения
    open class var colorPrimary: UIColor {
        return UIColor("#3c465a")
    }
    
    // Основной темный цвет приложения
    open class var colorPrimaryDark: UIColor {
        return UIColor("#3c465a")
    }
    
    // Основной акцент цвет приложения
    open class var colorAccent: UIColor {
        return UIColor("#be9682")
    }
    
    // Основной цвет фона
    open class var colorBackground: UIColor {
        return UIColor("#ffffff")
    }
    
    // Основной цвет темного фона, разделительные линии
    open class var colorBackgroundDark: UIColor {
        return UIColor("#fafafa")
    }
    
    // Цвет фона заголовка
    open class var colorBackgroundCaptionGroupView: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет фона заголовка
    open class var colorBackgroundToolbar: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет флага скидки
    open class var colorBackgroundFlagDiscount: UIColor {
        return UIColor("#3c465a")
    }
    
    // Цвет фона основного меню
    open class var colorBackgroundNav: UIColor {
        return UIColor("#fafafa")
    }
    
    // Стандартный цвет текста во всем проекте
    open class var colorTextPrimary: UIColor {
        return UIColor("#3c465a")
    }
    
    // Вспомогательный цвет текста во всем проекте
    open class var colorTextSecond: UIColor {
        return UIColor("#828282")
    }
    
    // Акцент цвет текста во всем проекте
    open class var colorTextAccent: UIColor {
        return UIColor("#be9682")
    }
    
    // Супер Акцент цвет текста во всем проекте
    open class var colorTextSuperAccent: UIColor {
        return UIColor("#be9682")
    }
    
    // Белый цвет текста
    open class var colorTextWhite: UIColor {
        return UIColor("#ffffff")
    }
    
    // Цвет текста в пунктах меню
    open class var colorTextNavMenu: UIColor {
        return UIColor("#3c465a")
    }
    
    // Цвет цены товара
    open class var colorTextPrice: UIColor {
        return UIColor("#0054a6")
    }
    
    // Цвет цены со скидкой
    open class var colorTextDiscountPrice: UIColor {
        return UIColor("#0054a6")
    }
    
    // Цвет старой цены
    open class var colorTextOldPrice: UIColor {
        return UIColor("#828282")
    }
    
    // Цвет процента скидки на бирке со скидкой
    open class var colorTextFlagDiscount: UIColor {
        return UIColor("#be9682")
    }
    
    // Цвет текста заголовка на подборке товара
    open class var colorTextCaptionGroupView: UIColor {
        return UIColor("#3c465a")
    }
    
    // Стандартный цвет текста загловка
    open class var colorTextToolbarTitle: UIColor {
        return UIColor("#3c465a")
    }
    
    // Стандартный цвет вспомогательного текста загловка
    open class var colorTextToolbarSecond: UIColor {
        return UIColor("#828282")
    }
    
    // Цвет фона баллов, если рейтинг выше 4
    open class var colorTextRating4: UIColor {
        return UIColor("#be9682")
    }
    
    // Цвет фона баллов, если рейтинг выше 3
    open class var colorTextRating3: UIColor {
        return UIColor("#828282")
    }
    
    // Цвет фона баллов, если рейтинг ниже 3
    open class var colorTextRating2: UIColor {
        return UIColor("#ed565b")
    }
    
    // Стандартный цвет текста на кнопке
    open class var colorButtonTextDefault: UIColor {
        return UIColor("#ffffff")
    }
    
    // Стандартный цвет текста на кнопке типа link
    open class var colorButtonTextPrimaryLink: UIColor {
        return UIColor("#3c465a")
    }
    
    // Стандартный цвет текста на кнопке типа link, accent стиля
    open class var colorButtonTextAccentLink: UIColor {
        return UIColor("#be9682")
    }
    
    // Стандартный цвет кнопки
    open class var colorButtonPrimary: UIColor {
        return UIColor("#3c465a")
    }
    
    // Стандартный цвет при нажатии
    open class var colorButtonPrimaryLite: UIColor {
        return UIColor("#3c465a")
    }
    
    // Стандартный цвет кнопки accent
    open class var colorButtonAccent: UIColor {
        return UIColor("#be9682")
    }
    
    // Стандартный цвет кнопки accent при нажатии
    open class var colorButtonAccentLite: UIColor {
        return UIColor("#be9682")
    }
    
    // Стандартный цвет всех иконок в приложении
    open class var colorIconPrimary: UIColor {
        return UIColor("#be9682")
    }
    
    // Стандартный цвет выделенной иконки
    open class var colorIconSelect: UIColor {
        return UIColor("#FFFFFF")
    }
    
    // Стандартный акцент цвет иконки
    open class var colorIconAccent: UIColor {
        return UIColor("#0054a6")
    }
    
    open class var colorIconSecond: UIColor {
        return UIColor("#828282")
    }
    
    // Стандартный цвет иконок в меню навигации
    open class var colorIconNavMenu: UIColor {
        return UIColor("#be9682")
    }
    
}
