
import Foundation
import FBSDKLoginKit
import VK_ios_sdk

class AppManager: NSObject {
    
    static let shared: AppManager = {
        return AppManager()
    }()
    
    var options: OptionsModel!
    
    var catalog: CatalogModel! {
        get {
            return options.directory.catalog
        }
    }
    
    var newsList: NewsModel!
    private var user: UserModel?
    var currentUser: UserModel? {
        get { return user }
        set {
            let userDefaults = UserDefaults.standard
            if newValue != nil {
                userDefaults.set(newValue!.info.token, forKey: "token")
                userDefaults.synchronize()
            } else {
                userDefaults.removeObject(forKey: "token")
                userDefaults.synchronize()
                VKSdk.forceLogout()
                FBSDKLoginManager().logOut()
            }
            user = newValue
        }
    }
    
}
