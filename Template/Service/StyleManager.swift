
import UIKit
import SVProgressHUD
import SideMenu

class StyleManager {
    
    static let shared = StyleManager.init()
    
    func styleAppearance() {
        SideMenuManager.menuFadeStatusBar = false
        prepareNavigationBarAppearance()
        prepareProgressHUD()
        proxyBodyLabel()
        proxyMainLabel()
        proxyAccentLabel()
        proxyEnterButton()
        proxyTextField()
        proxyBackgroundView()
        proxyPrimaryButton()
        proxyPrimaryLinkButton()
        proxyAccentButton()
        proxyAccentLinkButton()
        proxySwitch()
        proxySegmentControl()
        proxyEmptyButton()
        proxyEmptyLabel()
        proxySecondLabel()
    }
    
    func prepareNavigationBarAppearance() {
        //UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.colorBackgroundToolbar
        UINavigationBar.appearance().tintColor = UIColor.colorTextToolbarTitle
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.colorTextToolbarTitle]
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.colorPrimary],
                                                         for: .selected)
    }
    
    func prepareProgressHUD() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setMinimumDismissTimeInterval(1)
        SVProgressHUD.setMaximumDismissTimeInterval(1)
    }

    func proxyBodyLabel() {
        let proxy = BodyLabel.appearance()
        proxy.textColor = UIColor.colorTextSecond
        proxy.font = UIFont.systemFont(ofSize: 14)
    }
    
    func proxyAccentLabel() {
        let proxy = AccentLabel.appearance()
        proxy.textColor = UIColor.colorTextSecond
    }
    
    func proxySecondLabel() {
        let proxy = SecondLabel.appearance()
        proxy.textColor = UIColor.colorTextAccent
    }

    func proxyMainLabel() {
        let proxy = MainLabel.appearance()
        proxy.textColor = UIColor.colorTextPrimary
    }

    func proxyEnterButton() {
        let proxy = EnterButton.appearance()
        proxy.setTitleColor(UIColor.colorTextPrimary, for: .normal)
        proxy.titleLabelFont = UIFont.boldSystemFont(ofSize: 14)
        proxy.backgroundColor = UIColor.colorBackgroundDark
    }

    func proxyPrimaryButton() {
        let proxy = PrimaryButton.appearance()
        proxy.setTitleColor(UIColor.colorButtonTextDefault, for: .normal)
        proxy.backgroundColor = UIColor.colorButtonPrimary
    }
    
    func proxyPrimaryLinkButton() {
        let proxy = PrimaryLinkButton.appearance()
        proxy.setTitleColor(UIColor.colorButtonTextPrimaryLink, for: .normal)
        proxy.backgroundColor = UIColor.colorBackground
    }
    
    func proxyAccentButton() {
        let proxy = AccentButton.appearance()
        proxy.setTitleColor(UIColor.colorButtonTextDefault, for: .normal)
        proxy.backgroundColor = UIColor.colorButtonAccent
    }
    
    func proxyAccentLinkButton() {
        let proxy = AccentLinkButton.appearance()
        proxy.setTitleColor(UIColor.colorButtonTextAccentLink, for: .normal)
        proxy.backgroundColor = UIColor.colorBackground
    }
    
    func proxyTextField() {
        let proxyTextField = TextField.appearance()
        proxyTextField.placeholderColor = UIColor.colorTextSecond
        proxyTextField.activePlaceholderColor = UIColor.colorAccent
        proxyTextField.lineColor = UIColor.colorAccent
        proxyTextField.textColor = UIColor.colorTextPrimary
    }
    
    func proxyBackgroundView() {
        let proxy = BackgroundView.appearance()
        proxy.backgroundColor = UIColor.colorBackgroundDark
    }
    
    func proxySwitch() {
        let proxy = UISwitch.appearance()
        proxy.onTintColor = UIColor.colorAccent
    }
    
    func proxySegmentControl() {
        let proxy = UISegmentedControl.appearance()
        proxy.tintColor = UIColor.colorPrimary
    }
    
    func proxyEmptyButton() {
        let proxy = EmptyButton.appearance()
        proxy.tintColor = UIColor.colorIconSecond
    }

    func proxyEmptyLabel() {
        let proxy = EmptyLabel.appearance()
        proxy.textColor = UIColor.colorIconSecond
        proxy.numberOfLines = 0
        proxy.textAlignment = .center
        proxy.titleLabelFont = UIFont.systemFont(ofSize: 15)
    }
}
