

struct SettingsManager {
    
    /// разрешить лайк(дизлайк) товаров
    static let allowLikeDislikeProducts = true

    /// количество показываемых цен в карточке товара
    static let pricesCount = 1

    /// показывать скидки в карточке товара
    static let showSaleOnProducts = true

    /// стиль отображения товаров в корзине (есть 2 варианта)
    static let shoppingCartVariant1 = true

   
    /// MARK: Заказ
    /// показывать получателя
    static let showRecipientView = true
    
    /// показывать комментарий к заказу
    static let showCommentView = true
    
    /// показывать дату достаки
    static let showDateView = true
    
    /// пользователь обязательный или нет
    static let recipientIsRequired = false
    
    /// комментарий обязательный или нет
    static let commentIsRequired = false
    
    /// дата обязательная или нет
    static let dateIsRequired = false
    
    
    /// выбирить город доставки при первом запуске
    var showSelectStartCity: Bool {
        get {
            return AppManager.shared.options!.settings.selectStartCity && AppManager.shared.options!.settings.changeCity
        }
    }

    /// показывать город доставки
    static var showDeliveryCity: Bool {
        get {
            return AppManager.shared.options!.home.showCityDelivery
        }
    }

    /// разрешать изменение города доставки
    static var allowChangeDeliveryCity: Bool {
        get {
            return AppManager.shared.options!.settings.changeCity
        }
    }
    
    /// показывать социальные сеты
    static var showSocialLinks: Bool {
        get {
            return AppManager.shared.options!.home.showSocialLinks
        }
    }

    /// показывать недавно просмотренные товары
    static var showWatchedProducts: Bool {
        get {
            return AppManager.shared.options!.home.showWatchedProducts && DataManager.getViewedProducts().count > 0
        }
    }
    
    /// MARK: Info
    static let showOperatorButton = true
    static let showFeedbackButton = false
    static let showWriteUsButton = true


}
