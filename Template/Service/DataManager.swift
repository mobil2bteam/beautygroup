
import Foundation
import RealmSwift

let uiRealm = try! Realm()

class DataManager {
    
    static let productsCountLimit = 25
    static let viewedProductsKey = "DataManager.viewedProducts"
    static let favoriteProductsKey = "DataManager.favoriteProducts"

    class func addProductToViewedProducts(product: ProductModel) {
        if let p = uiRealm.object(ofType: ProductDataModel.self, forPrimaryKey: product.id) {
            try! uiRealm.write{
                p.date = Date()
            }
        } else {
            let newProduct = ProductDataModel()
            newProduct.isFavorite = false
            newProduct.image = product.images.first?.url ?? ""
            newProduct.name = product.name
            newProduct.id = product.id
            try! uiRealm.write{
                uiRealm.add(newProduct)
            }
        }
        if getViewedProducts().count > productsCountLimit {
            try! uiRealm.write{
                uiRealm.delete(getViewedProducts().first!)
            }
        }
    }
    
    class func getViewedProducts() -> [ProductDataModel] {
        let results = uiRealm.objects(ProductDataModel.self).sorted(byKeyPath: "date", ascending: false)
        return Array(results)
    }

    // MARK: Favorite products
    class func getFavoriteProducts() -> [Int] {
        let userDefaults = UserDefaults.standard
        if let products = userDefaults.array(forKey: favoriteProductsKey) as? [Int] {
            return products.reversed()
        } else {
            return []
        }
    }

    class func favoriteProduct(productId: Int) {
        let userDefaults = UserDefaults.standard
        if var products = userDefaults.array(forKey: favoriteProductsKey) as? [Int] {
            if let index = products.index(of: productId) {
                products.remove(at: index)
            } else {
                products.append(productId)
            }
            if products.count > 0 {
                userDefaults.set(products, forKey: favoriteProductsKey)
            } else {
                userDefaults.removeObject(forKey: favoriteProductsKey)
            }
        } else {
            userDefaults.set([productId], forKey: favoriteProductsKey)
        }
        userDefaults.synchronize()
    }
    
    class func favoriteProductIsAdded(productId: Int) -> Bool {
        let userDefaults = UserDefaults.standard
        if let products = userDefaults.array(forKey: favoriteProductsKey) as? [Int] {
            return products.contains(productId)
        }
        return false
    }
    

}
